<?php
require("../vendor/autoload.php");
$swagger = \Swagger\scan('../app/controllers');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Content-Type: application/json');
echo $swagger;
