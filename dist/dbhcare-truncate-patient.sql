SET foreign_key_checks = 0;
TRUNCATE TABLE `admission_order`;
TRUNCATE TABLE `admission_order_communication_type`;
TRUNCATE TABLE `admission_order_eval_treatment`;
TRUNCATE TABLE `admission_order_sn_monitor`;
TRUNCATE TABLE `allergy_profile`;
TRUNCATE TABLE `case_30_days`;
TRUNCATE TABLE `case_conference`;
TRUNCATE TABLE `case_conference_intermed`;
TRUNCATE TABLE `case_discharge_ins`;
TRUNCATE TABLE `case_summary`;
TRUNCATE TABLE `case_summary_types`;
TRUNCATE TABLE `cc_attachments`;
TRUNCATE TABLE `cc_missed_visits`;
TRUNCATE TABLE `cc_missed_visits_coordinators`;
TRUNCATE TABLE `cc_notes`;
TRUNCATE TABLE `cc_notes_coordinators`;
TRUNCATE TABLE `commcoordination_comments`;
TRUNCATE TABLE `discharge_order`;
TRUNCATE TABLE `discharge_order_communication_type`;
TRUNCATE TABLE `discharge_order_coordinators`;
TRUNCATE TABLE `discharge_summary`;
TRUNCATE TABLE `discharge_summary_options`;
TRUNCATE TABLE `document`;
TRUNCATE TABLE `face2face`;
TRUNCATE TABLE `medications`;
TRUNCATE TABLE `medication_attachment`;
TRUNCATE TABLE `medication_profile`;
TRUNCATE TABLE `medication_reconciliation`;
TRUNCATE TABLE `medication_teachings`;
TRUNCATE TABLE `miscellaneous`;
TRUNCATE TABLE `misc_comments`;
TRUNCATE TABLE `msw_emostat`;
TRUNCATE TABLE `msw_folupplan`;
TRUNCATE TABLE `msw_mentalstatus`;
TRUNCATE TABLE `msw_protowgoa`;
TRUNCATE TABLE `msw_visitgoals`;
TRUNCATE TABLE `msw_visitinterventions`;
TRUNCATE TABLE `notes`;
TRUNCATE TABLE `notes_aide_note`;
TRUNCATE TABLE `note_msw_visit`;
TRUNCATE TABLE `oasis`;
TRUNCATE TABLE `oasis_adl`;
TRUNCATE TABLE `oasis_cardio`;
TRUNCATE TABLE `oasis_cardio_absent`;
TRUNCATE TABLE `oasis_cardio_breathchar`;
TRUNCATE TABLE `oasis_cardio_chestpain`;
TRUNCATE TABLE `oasis_cardio_diminished`;
TRUNCATE TABLE `oasis_cardio_rad`;
TRUNCATE TABLE `oasis_cardio_rales`;
TRUNCATE TABLE `oasis_cardio_rhonchi`;
TRUNCATE TABLE `oasis_cardio_wheezes`;
TRUNCATE TABLE `oasis_care`;
TRUNCATE TABLE `oasis_clinical`;
TRUNCATE TABLE `oasis_diagnosis`;
TRUNCATE TABLE `oasis_elimination`;
TRUNCATE TABLE `oasis_elim_complain`;
TRUNCATE TABLE `oasis_elim_lowergas`;
TRUNCATE TABLE `oasis_elim_ostocare`;
TRUNCATE TABLE `oasis_elim_ostomycare`;
TRUNCATE TABLE `oasis_elim_peritoneal`;
TRUNCATE TABLE `oasis_elim_stoolchar`;
TRUNCATE TABLE `oasis_elim_timeweek`;
TRUNCATE TABLE `oasis_elim_urineapp`;
TRUNCATE TABLE `oasis_elim_urinecont`;
TRUNCATE TABLE `oasis_emergent`;
TRUNCATE TABLE `oasis_endcurine`;
TRUNCATE TABLE `oasis_endu_bloodsugar`;
TRUNCATE TABLE `oasis_endu_cbs`;
TRUNCATE TABLE `oasis_endu_diaman`;
TRUNCATE TABLE `oasis_endu_foot`;
TRUNCATE TABLE `oasis_endu_hyper`;
TRUNCATE TABLE `oasis_endu_hypog`;
TRUNCATE TABLE `oasis_endu_insuline`;
TRUNCATE TABLE `oasis_endu_lekodi`;
TRUNCATE TABLE `oasis_general`;
TRUNCATE TABLE `oasis_gen_ap`;
TRUNCATE TABLE `oasis_gen_fl`;
TRUNCATE TABLE `oasis_gen_hh`;
TRUNCATE TABLE `oasis_gen_hobo`;
TRUNCATE TABLE `oasis_gen_sm`;
TRUNCATE TABLE `oasis_integumentary`;
TRUNCATE TABLE `oasis_int_careorder`;
TRUNCATE TABLE `oasis_int_skin`;
TRUNCATE TABLE `oasis_int_wound`;
TRUNCATE TABLE `oasis_int_woundfield`;
TRUNCATE TABLE `oasis_int_woundnum`;
TRUNCATE TABLE `oasis_m0140`;
TRUNCATE TABLE `oasis_m0150`;
TRUNCATE TABLE `oasis_m1000`;
TRUNCATE TABLE `oasis_m1018`;
TRUNCATE TABLE `oasis_m1021`;
TRUNCATE TABLE `oasis_m1028`;
TRUNCATE TABLE `oasis_m1030`;
TRUNCATE TABLE `oasis_m1033`;
TRUNCATE TABLE `oasis_m1034`;
TRUNCATE TABLE `oasis_m1036`;
TRUNCATE TABLE `oasis_m1410`;
TRUNCATE TABLE `oasis_m1511`;
TRUNCATE TABLE `oasis_m1740`;
TRUNCATE TABLE `oasis_m2310`;
TRUNCATE TABLE `oasis_m2430`;
TRUNCATE TABLE `oasis_mascoskeletal`;
TRUNCATE TABLE `oasis_mascu_skeletal`;
TRUNCATE TABLE `oasis_medical`;
TRUNCATE TABLE `oasis_medication`;
TRUNCATE TABLE `oasis_med_intravenous`;
TRUNCATE TABLE `oasis_neuro`;
TRUNCATE TABLE `oasis_neuro_disoriented`;
TRUNCATE TABLE `oasis_neuro_learning`;
TRUNCATE TABLE `oasis_neuro_levelcon`;
TRUNCATE TABLE `oasis_neuro_observation`;
TRUNCATE TABLE `oasis_neuro_sign`;
TRUNCATE TABLE `oasis_neuro_signabuse`;
TRUNCATE TABLE `oasis_nutritional`;
TRUNCATE TABLE `oasis_nutri_abdomen`;
TRUNCATE TABLE `oasis_nutri_enternal`;
TRUNCATE TABLE `oasis_nutri_nutrition`;
TRUNCATE TABLE `oasis_nutri_ostomy`;
TRUNCATE TABLE `oasis_nutri_screen`;
TRUNCATE TABLE `oasis_pmh_cardio`;
TRUNCATE TABLE `oasis_pmh_circu`;
TRUNCATE TABLE `oasis_pmh_endocrine`;
TRUNCATE TABLE `oasis_pmh_gani`;
TRUNCATE TABLE `oasis_pmh_gastro`;
TRUNCATE TABLE `oasis_pmh_integ`;
TRUNCATE TABLE `oasis_pmh_muscu`;
TRUNCATE TABLE `oasis_pmh_neuro`;
TRUNCATE TABLE `oasis_pmh_sensory`;
TRUNCATE TABLE `oasis_sensory`;
TRUNCATE TABLE `oasis_site1_acc_lev`;
TRUNCATE TABLE `oasis_site1_lev_af_med`;
TRUNCATE TABLE `oasis_site1_pres_lev`;
TRUNCATE TABLE `oasis_site1_worst_lev`;
TRUNCATE TABLE `oasis_site2_acc_lev`;
TRUNCATE TABLE `oasis_site2_lev_af_med`;
TRUNCATE TABLE `oasis_site2_pres_lev`;
TRUNCATE TABLE `oasis_site2_worst_lev`;
TRUNCATE TABLE `oasis_ss_mouth`;
TRUNCATE TABLE `oasis_ss_non_ver`;
TRUNCATE TABLE `oasis_ss_nose`;
TRUNCATE TABLE `oasis_ss_pain_scale`;
TRUNCATE TABLE `oasis_ss_speech`;
TRUNCATE TABLE `oasis_ss_throat`;
TRUNCATE TABLE `oasis_ss_type_pain`;
TRUNCATE TABLE `oasis_ss_wmpb`;
TRUNCATE TABLE `oasis_theraphy`;
TRUNCATE TABLE `oasis_thera_communicated`;
TRUNCATE TABLE `oasis_thera_coordination`;
TRUNCATE TABLE `oasis_thera_dischargeplan`;
TRUNCATE TABLE `oasis_thera_instruction`;
TRUNCATE TABLE `oasis_thera_medication`;
TRUNCATE TABLE `oasis_thera_msw`;
TRUNCATE TABLE `oasis_thera_obtainphy`;
TRUNCATE TABLE `oasis_thera_occutheraphy`;
TRUNCATE TABLE `oasis_thera_patientcon`;
TRUNCATE TABLE `oasis_thera_phytheraphy`;
TRUNCATE TABLE `oasis_thera_speech`;
TRUNCATE TABLE `oasis_thera_sumskill`;
TRUNCATE TABLE `orders`;
TRUNCATE TABLE `order_attachment`;
TRUNCATE TABLE `otc_note`;
TRUNCATE TABLE `otc_note_carecoordination`;
TRUNCATE TABLE `otc_note_clinicalfindings`;
TRUNCATE TABLE `otc_note_psychosocial`;
TRUNCATE TABLE `otc_note_ptinterventions`;
TRUNCATE TABLE `patientadmission`;
TRUNCATE TABLE `patientauthorization`;
TRUNCATE TABLE `patientauthorizationfreq`;
TRUNCATE TABLE `patientcaredocstatus`;
TRUNCATE TABLE `patientepisode`;
TRUNCATE TABLE `patientepisodetask`;
TRUNCATE TABLE `patientepisodetaskcomments`;
TRUNCATE TABLE `patientepisodetaskfiles`;
TRUNCATE TABLE `patientepisodetaskmissed`;
TRUNCATE TABLE `patientepisodetaskmissedcoordinators`;
TRUNCATE TABLE `patients_medication`;
TRUNCATE TABLE `patients_medication_history`;
TRUNCATE TABLE `patients_medication_reconciliation`;
TRUNCATE TABLE `patient_form`;
TRUNCATE TABLE `patient_record_file`;
TRUNCATE TABLE `physicianorder`;
TRUNCATE TABLE `physicianorderattachment`;
TRUNCATE TABLE `physicianordercoordinator`;
TRUNCATE TABLE `physicianorderfreq`;
TRUNCATE TABLE `physicianorder_justification`;
TRUNCATE TABLE `physicianrecurring`;
TRUNCATE TABLE `physician_justification_order_coordinators`;
TRUNCATE TABLE `planofcare`;
TRUNCATE TABLE `ptc_note`;
TRUNCATE TABLE `ptc_note_carecoordination`;
TRUNCATE TABLE `ptc_note_psychosocial`;
TRUNCATE TABLE `ptc_note_ptinterventions`;
TRUNCATE TABLE `pt_evaluation_assistivedevice`;
TRUNCATE TABLE `pt_evaluation_funcstatambssc`;
TRUNCATE TABLE `pt_evaluation_funcstatbedcom`;
TRUNCATE TABLE `pt_evaluation_funcstatrolling`;
TRUNCATE TABLE `pt_evaluation_funcstatscooting`;
TRUNCATE TABLE `pt_evaluation_funcstatshotub`;
TRUNCATE TABLE `pt_evaluation_funcstatsitsta`;
TRUNCATE TABLE `pt_evaluation_funcstatsupsit`;
TRUNCATE TABLE `pt_evaluation_funcstattoilet`;
TRUNCATE TABLE `pt_evaluation_funcstatwhecha`;
TRUNCATE TABLE `pt_evaluation_gaiassdeviations`;
TRUNCATE TABLE `pt_evaluation_gaiassevesur`;
TRUNCATE TABLE `pt_evaluation_gaiassunesur`;
TRUNCATE TABLE `pt_evaluation_planofcare`;
TRUNCATE TABLE `pt_evaluation_poc`;
TRUNCATE TABLE `pt_evaluation_psychosocial`;
TRUNCATE TABLE `recert_order`;
TRUNCATE TABLE `recert_order_communication_type`;
TRUNCATE TABLE `recert_order_eval_treatment`;
TRUNCATE TABLE `recert_order_sn_monitor`;
TRUNCATE TABLE `roc_order`;
TRUNCATE TABLE `roc_order_communication_type`;
TRUNCATE TABLE `roc_order_eval_treatment`;
TRUNCATE TABLE `roc_order_sn_monitor`;
TRUNCATE TABLE `sixty_days_summary`;
TRUNCATE TABLE `sixty_days_summary_services`;
TRUNCATE TABLE `snv`;
TRUNCATE TABLE `snv_card_heartrhythm`;
TRUNCATE TABLE `snv_dme_infectioncont`;
TRUNCATE TABLE `snv_dme_inspection`;
TRUNCATE TABLE `snv_gan_ganitourinary`;
TRUNCATE TABLE `snv_gan_hemodialysis`;
TRUNCATE TABLE `snv_gan_urine`;
TRUNCATE TABLE `snv_gas_abdomen`;
TRUNCATE TABLE `snv_gas_stool`;
TRUNCATE TABLE `snv_integ_color`;
TRUNCATE TABLE `snv_integ_lession`;
TRUNCATE TABLE `snv_integ_skin`;
TRUNCATE TABLE `snv_int_intravenous`;
TRUNCATE TABLE `snv_int_ivmedication`;
TRUNCATE TABLE `snv_med_carecoor`;
TRUNCATE TABLE `snv_med_medcompli`;
TRUNCATE TABLE `snv_neu_oriented`;
TRUNCATE TABLE `snv_nut_appetite`;
TRUNCATE TABLE `snv_nut_diet`;
TRUNCATE TABLE `snv_nut_tube`;
TRUNCATE TABLE `snv_psy_homebound`;
TRUNCATE TABLE `snv_psy_psycho`;
TRUNCATE TABLE `special_procedures`;
TRUNCATE TABLE `sp_av_shunt`;
TRUNCATE TABLE `sp_catheter`;
TRUNCATE TABLE `sp_enteral_feeding`;
TRUNCATE TABLE `sp_ostomy`;
TRUNCATE TABLE `sp_pacemaker`;
TRUNCATE TABLE `sp_peritoneal_dialysis`;
TRUNCATE TABLE `sp_ventilator`;
TRUNCATE TABLE `stc_note`;
TRUNCATE TABLE `stc_note_carecoordination`;
TRUNCATE TABLE `stc_note_methods`;
TRUNCATE TABLE `stc_note_psychosocial`;
TRUNCATE TABLE `stc_note_ptinterventions`;
TRUNCATE TABLE `st_evaluation`;
TRUNCATE TABLE `st_evaluation_planofcare`;
TRUNCATE TABLE `st_evaluation_poc`;
TRUNCATE TABLE `st_evaluation_psychosocial`;
TRUNCATE TABLE `supervisory_visit`;
TRUNCATE TABLE `sv`;
TRUNCATE TABLE `sv_card_heartrhythm`;
TRUNCATE TABLE `sv_dme_infectioncont`;
TRUNCATE TABLE `sv_dme_inspection`;
TRUNCATE TABLE `sv_gan_ganitourinary`;
TRUNCATE TABLE `sv_gan_hemodialysis`;
TRUNCATE TABLE `sv_gan_urine`;
TRUNCATE TABLE `sv_gas_abdomen`;
TRUNCATE TABLE `sv_gas_stool`;
TRUNCATE TABLE `sv_integ_color`;
TRUNCATE TABLE `sv_integ_lession`;
TRUNCATE TABLE `sv_integ_skin`;
TRUNCATE TABLE `sv_int_intravenous`;
TRUNCATE TABLE `sv_int_ivmedication`;
TRUNCATE TABLE `sv_med_carecoor`;
TRUNCATE TABLE `sv_med_medcompli`;
TRUNCATE TABLE `sv_neu_oriented`;
TRUNCATE TABLE `sv_nut_appetite`;
TRUNCATE TABLE `sv_nut_diet`;
TRUNCATE TABLE `sv_nut_tube`;
TRUNCATE TABLE `sv_pain_assessment`;
TRUNCATE TABLE `sv_psy_homebound`;
TRUNCATE TABLE `sv_psy_psycho`;
TRUNCATE TABLE `table_comment`;
TRUNCATE TABLE `transfer_order`;
TRUNCATE TABLE `transfer_order_communication_type`;
TRUNCATE TABLE `transfer_order_coordinators`;
TRUNCATE TABLE `wound_care`;
TRUNCATE TABLE `wound_care_episode`;
TRUNCATE TABLE `wound_care_image`;
TRUNCATE TABLE `wound_care_woundfield`;
TRUNCATE TABLE `wound_care_woundnum`;

UPDATE `patient` SET
`status` = 'PENDING',
`dateUpdated` = now();

UPDATE `patient` SET
`admission` = '0',
`dateUpdated` = now();

