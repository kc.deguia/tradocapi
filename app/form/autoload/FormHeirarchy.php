<?php
namespace Form\Heirarchy;

/**
 * Author : Kyben de Guia
 */
class FormHeirarchy{
    /*
    Map Module Structure Heirarchy
    => MODULE
        => SECTION
            => FROM VERSIONS
    */
    public static function modules(){
        return  array(
            "oasis" => array(
                "oasisClinical" => array(
                    "v1" => "OasisClinicalV1",
                )
            )
        );
    }

}
