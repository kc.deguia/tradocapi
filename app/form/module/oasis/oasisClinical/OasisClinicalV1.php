<?php
namespace Form\Module;

use Form\Stat\OasisClinicalV1 as Stat;
/**
 * OASIS CLINICAL RECORD FORM CONSTANT
 */
class OasisClinicalV1
{

    public static function test(){
        return Stat::cons();

    }

}
