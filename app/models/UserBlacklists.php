<?php

namespace Models;

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Db\Column;

class UserBlacklists extends Basemodel {

    public function initialize() {
    }

    public static function search($arg, $col=["*"], $bind){

		$col = implode(',', $col);

        // A raw SQL statement
        $sql = "SELECT $col FROM user_blacklists
				INNER JOIN members ON members.memberid = user_blacklists.userid
    			WHERE $arg";

        // Base model
        $sq = new UserBlacklists();

        // Execute the query
        $query = $sq->getReadConnection()->query($sql, $bind);
        $result = new Resultset(null, $sq, $query);
        $data = $result->toArray();
        return $data;
    }

}
