<?php

namespace LibraryModels;

use Services\PaginatorQuery;
use Models\Basemodel;

class Documents extends Basemodel
{
	public static function getDirectories() {
		$query  = " SELECT * FROM document_directories WHERE 1 ORDER BY sequence ASC";

		$paginator = new PaginatorQuery([
			'query'	 => $query,
			'params' => [],
			'page'	 => 1, 
			'limit'	 => 2147483647
		]);

		$paginator = $paginator->withoutTrashedFrom(['document_directories']);
		return $paginator->getPaginate();
	}

	public static function getDirectoryFiles($directory_id) {
		$query  = " SELECT * FROM document_files WHERE directory_id = :id ORDER BY sequence ASC";
		$params = [
            ['keyword' => ':id', 'value' => $directory_id, 'type' => 'str']
        ];

		$paginator = new PaginatorQuery([
			'query'	 => $query,
			'params' => $params,
			'page'	 => 1, 
			'limit'	 => 2147483647
		]);

		$paginator = $paginator->withoutTrashedFrom(['document_files']);
		return $paginator->getPaginate();
	}
}