<?php

namespace Controllers;

use Models\Members;
use Models\Memberroles;
use Models\Membersfile;
use Models\Membersdirectory;
use Models\Roletemplate;
use Models\Roleitems;
use Models\Rolegroups;
use Models\Roletemplateitems;
use Models\Membersecurityquestions;
use Models\Securityquestions;
use Models\ForgotPassword;

use Controllers\ControllerBase as CB;
use Controllers\AmazonController as AC;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Manager;
use Utilities\Guid\Guid;
use Security\Jwt\JWT;
use Security\AclRoles;
use S3;
use RequestValidator\Member as CMValidator;
use RequestValidator\Esign as Esign;
use RequestValidator\Username;
use RequestValidator\SetSecurity as SS;
use RequestValidator\UserEmail;
use RequestValidator\SSN;
use RequestValidator\RequestResetPW;
use RequestValidator\NewPasswordReset;
use Micro\Exceptions\HTTPExceptions;
use RequestValidator\Custom\Uniquememberdata;

use Constants\RolesConstants;
use Constants\DocStatus;
use Constants\OrderConstants;

use Models\Members_professional_credentials as ProdCred;
use Models\Members_health_credentials as HpCred;
use Models\Members_language as MLang;

class MembersController extends ControllerBase {

    protected $errors;

    protected $_profileUpdate = false;

    protected $_member = [];

    protected $_memberlanguage = [];

    protected $_constantRole = [
        "a1"
    ];

    protected $attempt = 0;

    public function addMember(){


        $guid = new Guid();
            $this->validationResponseError(new CMValidator(), $_POST,[
                "username" => new Uniquememberdata(['message' => 'Username already taken by another user.','model' => '\Models\Members']),
                "email" => new Uniquememberdata(['message' => 'Email already taken by another user.','model' => '\Models\Members']),
                "ssn" => new Uniquememberdata(['message' => 'SSN already taken by another user.','model' => '\Models\Members'])
                ]);


            $password = CB::randomPassword();
            $this->_member['memberid']      = $guid->GUID();
            $this->_member['username']      = $this->request->getPost('username',['alphanum','trim']);
            $this->_member['email']         = $this->request->getPost('email', ['email','trim']);
            $this->_member['password']      = $this->security->hash($password);
            $this->_member['firstname']     = $this->request->getPost('firstname', ['string','trim']);
            $this->_member['lastname']      = $this->request->getPost('lastname', ['string','trim']);
            $this->_member['suffix']        = $this->request->getPost('suffix', ['string','trim']);
            $this->_member['birthdate']     = $this->request->getPost('birthdate');
            $this->_member['gender']        = $this->request->getPost('gender', ['string','trim']);
            $this->_member['credential']    = $this->request->getPost('credential', ['string','trim']);
            $this->_member['title']         = $this->request->getPost('title', ['string','trim']);
            $this->_member['employeetype']  = $this->request->getPost('employeetype', ['string','trim']);
            $this->_member['ssn']           = $this->request->getPost('ssn', ['string','trim']);
            $this->_member['employeestatus']= $this->request->getPost('employeestatus', ['string','trim']);
            $this->_member['employeeid']    = $this->request->getPost('employeeid', ['string','trim']);
            $this->_member['address1']      = $this->request->getPost('address1', ['string','trim']);
            $this->_member['address2']      = $this->request->getPost('address2', ['string','trim']);
            $this->_member['city']          = $this->request->getPost('city', ['string','trim']);
            $this->_member['state']         = $this->request->getPost('state', ['string','trim']);
            $this->_member['zip']           = $this->request->getPost('zip', ['string','trim']);
            $this->_member['phone']         = $this->request->getPost('phone', ['string','trim']);
            $this->_member['mobile']        = $this->request->getPost('mobile', ['string','trim']);
            $this->_member['remarks']       = $this->request->getPost('remarks', ['trim']);
            $this->_member['roles']         = $this->request->getPost('roles');
            /*RJ code below*/
            $this->_member['race_ethnicity']= $this->request->getPost('race_ethnicity', ['trim']);
            $this->_member['marital_status']= $this->request->getPost('marital_status', ['trim']);
            $this->_member['discipline']    = $this->request->getPost('discipline', ['trim']);
            $this->_member['fax']           = $this->request->getPost('fax', ['trim']);

            $this->_memberlanguage['language']      = $this->request->getPost('language');

            $this->_member['professionalCredentials'] = $this->request->getPost('professionalCredentials');
            $this->_member['healthCredentials']       = $this->request->getPost('healthCredentials');
            
            try {

                $savemember = new Members();
                $this->db->begin();

                if(!$savemember->save($this->_member)){
                    $this->errors = $this->errorObjectToArray($savemember->getMessages());
                    $this->db->rollback('Cannot save member.');
                }

                /*Members language[RJ]*/
                $mlang = MLang::insertData($this->_memberlanguage['language'], $this->_member['memberid']);
                if(is_object($mlang)){
                    $this->errors = $mlang->getMessage();
                    $this->db->rollback();
                }

                /*Professional Credentials[RJ]*/
                $proCred = ProdCred::insertData($this->_member['professionalCredentials'], $this->_member['memberid']);
                if(is_object($proCred)){
                    $this->errors = $proCred->getMessage();
                    $this->db->rollback();
                }

                /*Health Credentials[RJ]*/
                $hpCred = HpCred::insertData($this->_member['healthCredentials'], $this->_member['memberid']);
                if(is_object($hpCred)){
                    $this->errors = $hpCred->getMessage();
                    $this->db->rollback();
                }

                // $savelogininfo = new Logininfo();
                // $savelogininfo->setTransaction($transaction);

                // $savelogininfo->memberid = $memberid;
                // $savelogininfo->status = 'first';

                // if(!$savelogininfo->save()){
                //     $transaction->rollback();
                // }

                $this->_member['roles']['auth'] = $this->_constantRole;
                $res = Memberroles::insertMemberRoles($this->_member['roles'], $this->_member['memberid']);
                if(is_object($res)){
                    $this->errors = $res->getMessage();
                    $this->db->rollback();
                }

                //send account information to user via email
                $emailcontent = array(
                    'username' => $this->_member['username'],
                    'password' => $password
                );

                $mail = $this->Mailer->sendMail($this->_member['email'], 'Medisource :Account Login Information', $emailcontent, 'addTemplate');

                if($mail == 0){
                    $this->db->rollback("Email server does not accept your request.");
                }

                //save data to database if there is no error
                $this->db->commit();

                //Generate Role Cache for User
                $this->generateUserCacheAcl($this->_member['memberid']);

                //success message
                return array('Member have been saved.');
            }
            catch (\Exception $e) {
                $this->ErrorMessage->thrower(
                    406,
                    $e->getMessage(),
                    "Saving Errors",
                    "MEM002",
                    $this->errors
                );
            }
    } //end of addMember fucntion

    public function editMember(){

        parse_str(file_get_contents("php://input"), $put);

        if(empty($put['memberid'])){
            $this->ErrorMessage->thrower(
                    406,
                    "Current User is missing.",
                    "Updating Error",
                    "MEM002"
                );
        }

        $this->validationResponseError(new CMValidator(), $put,
            [
                "username" => new Uniquememberdata([
                        'message' => 'Username already taken by another user.',
                        'model' => '\Models\Members',
                        'exclude' => [
                            'con' => ' memberid != ?1 ',
                            'bind' => [$this->request->getPut('memberid',['trim'])]
                        ]
                ]),
                "email" => new Uniquememberdata([
                        'message' => 'Email already taken by another user.',
                        'model' => '\Models\Members',
                        'exclude' => [
                            'con' => ' memberid != ?1 ',
                            'bind' => [$this->request->getPut('memberid',['trim'])]
                        ]
                ])
                // SSN ADD UNIQUE
            ]);

            $this->_member['memberid']      = $this->request->getPut('memberid',['trim']);
            $this->_member['username']      = $this->request->getPut('username',['alphanum','trim']);
            $this->_member['email']         = $this->request->getPut('email', ['email','trim']);
            $this->_member['firstname']     = $this->request->getPut('firstname', ['string','trim']);
            $this->_member['lastname']      = $this->request->getPut('lastname', ['string','trim']);
            $this->_member['suffix']        = $this->request->getPut('suffix', ['string','trim']);
            $this->_member['birthdate']     = $this->request->getPut('birthdate');
            $this->_member['gender']        = $this->request->getPut('gender', ['string','trim']);
            $this->_member['credential']    = $this->request->getPut('credential', ['string','trim']);
            $this->_member['title']         = $this->request->getPut('title', ['string','trim']);
            $this->_member['employeetype']  = $this->request->getPut('employeetype', ['string','trim']);
            $this->_member['ssn']           = $this->request->getPut('ssn', ['string','trim']);
            $this->_member['employeestatus']= $this->request->getPut('employeestatus', ['string','trim']);
            $this->_member['employeeid']    = $this->request->getPut('employeeid', ['string','trim']);
            $this->_member['address1']      = $this->request->getPut('address1', ['string','trim']);
            $this->_member['address2']      = $this->request->getPut('address2', ['string','trim']);
            $this->_member['city']          = $this->request->getPut('city', ['string','trim']);
            $this->_member['state']         = $this->request->getPut('state', ['string','trim']);
            $this->_member['zip']           = $this->request->getPut('zip', ['string','trim']);
            $this->_member['phone']         = $this->request->getPut('phone', ['string','trim']);
            $this->_member['mobile']        = $this->request->getPut('mobile', ['string','trim']);
            $this->_member['remarks']       = $this->request->getPut('remarks', ['trim']);

            /*RJ code below*/
            $this->_member['race_ethnicity']= $this->request->getPut('race_ethnicity', ['trim']);
            $this->_member['marital_status']= $this->request->getPut('marital_status', ['trim']);
            $this->_member['discipline']    = $this->request->getPut('discipline', ['trim']);
            $this->_member['fax']           = $this->request->getPut('fax', ['trim']);

            
            if(!$this->_profileUpdate){
                $this->_member['roles']         = $this->request->getPut('roles');
                $this->_memberlanguage['language']      = $this->request->getPut('language');
                $this->_member['professionalCredentials'] = $this->request->getPut('professionalCredentials');
                $this->_member['healthCredentials']       = $this->request->getPut('healthCredentials');
            }

            try {

                $this->db->begin();
                $member = Members::findFirst(
                    ["memberid = ?0" ,
                    'bind' => [$this->_member['memberid']]
                    ]);

                if(!$member){
                    $this->ErrorMessage->thrower(
                        406,
                        "Current User is missing.",
                        "Updating Error",
                        "MEM002"
                    );
                }

                if(!$member->save($this->_member)){
                    $this->errors = $this->errorObjectToArray($member->getMessages());
                    $this->db->rollback('Cannot update member.');
                }

                if(!$this->_profileUpdate){
                    $deletememberrole = Memberroles::find(["memberid = ?0" , 'bind' => [$this->_member['memberid'] ]]);

                    if ($deletememberrole !== false) {
                        if ($deletememberrole->delete() === false) {
                            $this->errors = $this->errorObjectToArray($deletememberrole->getMessages());
                            $this->db->rollback('Cannot delete member roles member.');
                        }
                    }

                    $insertroles = Memberroles::insertMemberRoles($this->_member['roles'], $this->_member['memberid']);
                    if(is_object($insertroles)){
                        $this->errors = $insertroles->getMessage();
                        $this->db->rollback('Cannot insert new member roles.');
                    }
                    /*Members language[RJ]*/
                    $deleteMLang = MLang::find(["memberid = ?0" , 'bind' => [$this->_member['memberid'] ]]);
                    if ($deleteMLang !== false) {
                        if ($deleteMLang->delete() === false) {
                            $this->errors = $this->errorObjectToArray($deleteMLang->getMessages());
                            $this->db->rollback('Cannot delete member language.');
                        }
                    }
                    $mlang = MLang::insertData($this->_memberlanguage['language'], $this->_member['memberid']);
                    if(is_object($mlang)){
                        $this->errors = $mlang->getMessage();
                        $this->db->rollback('Cannot insert new member language.');
                    }
                    /*Professional Credentials[RJ]*/
                    $deletememberProdCred = ProdCred::find(["memberid = ?0" , 'bind' => [$this->_member['memberid'] ]]);
                    if ($deletememberProdCred !== false) {
                        if ($deletememberProdCred->delete() === false) {
                            $this->errors = $this->errorObjectToArray($deletememberProdCred->getMessages());
                            $this->db->rollback('Cannot delete member Professional Credentials.');
                        }
                    }
                    $proCred = ProdCred::insertData($this->_member['professionalCredentials'], $this->_member['memberid']);
                    if(is_object($proCred)){
                        $this->errors = $proCred->getMessage();
                        $this->db->rollback('Cannot insert new member Professional Credentials.');
                    }
                    $deletememberHpCred = HpCred::find(["memberid = ?0" , 'bind' => [$this->_member['memberid'] ]]);
                    /*Health Credentials[RJ]*/
                    if ($deletememberHpCred !== false) {
                        if ($deletememberHpCred->delete() === false) {
                            $this->errors = $this->errorObjectToArray($deletememberHpCred->getMessages());
                            $this->db->rollback('Cannot delete member Health Credentials.');
                        }
                    }
       
                    $hpCred = HpCred::insertData($this->_member['healthCredentials'], $this->_member['memberid']);
                    if(is_object($hpCred)){
                        $this->errors = $hpCred->getMessage();
                        $this->db->rollback('Cannot insert new member Health Credentials.');
                    }    
                }

                $this->db->commit();

                if(!$this->_profileUpdate){
                    //Generate Role Cache for User
                    $this->generateUserCacheAcl($this->_member['memberid']);

                    //Require User to Relogin
                    $this->redisWrapper->redisSAdd("hc_require_relogin", $this->_member['memberid']);
                    return array("Members has been updated and now is required to relogin.");
                }

                return array('Member have been saved.');
            }
            catch (\Exception $e) {
                $this->ErrorMessage->thrower(
                    406,
                    $e->getMessage(),
                    "Updating Errors",
                    "MEM002",
                    $this->errors
                );
            }

    }

    public function checkUsername(){

        $this->validationResponseError(new Username(), $_POST);

        $username = $this->request->getPost('username', ['trim']);
        $exclude = $this->request->getPost('exclude', ['trim']);

        $condition = " username = ?0 ";
        $bind = [$username];

        if(isset($exclude)){
            $condition .= " AND memberid != ?1";
            $bind[] = $exclude;
        }

        $checkusername =  Members::findFirst(
            [
                $condition,
                "bind" => $bind
            ]);

        if($checkusername){
            $this->ErrorMessage->userExist('Username already exist!');
        }
        return array('success' => 'Username is available!');
    }

    public function checkSSN(){

        $this->validationResponseError(new SSN(), $_POST);

        $username = $this->request->getPost('ssn', ['trim']);
        $exclude = $this->request->getPost('exclude', ['trim']);

        $condition = " ssn = ?0 ";
        $bind = [$username];

        if(isset($exclude)){
            $condition .= " AND memberid != ?1";
            $bind[] = $exclude;
        }

        $checkusername =  Members::findFirst(
            [
                $condition,
                "bind" => $bind
            ]);

        if($checkusername){
            $this->ErrorMessage->userExist('SSN already exist!');
        }
        return array('success' => 'SSN is available!');
    }


    public function checkEmail(){

        $this->validationResponseError(new UserEmail(), $_POST);

        $email = $this->request->getPost('email', ['email','trim']);
        $exclude = $this->request->getPost('exclude', ['email','trim']);

        $condition = " email = ?0 ";
        $bind = [$email];

        if(isset($exclude)){
            $condition .= " AND memberid != ?1";
            $bind[] = $exclude;
        }

        $checkemail =  Members::findFirst([
                $condition,
                "bind" => $bind
            ]);

        if($checkemail){
            $this->ErrorMessage->userExist('Email is already exist!');
        }

        return $data = array('success' => 'Email is available!');
    }

    public function loadMemberbyid($memberid){

        if(empty($memberid)){
            $this->ErrorMessage->thrower(
                    406,
                    "Empty Member ID",
                    "Empty Member ID",
                    "MEM002"
                );
        }

        $member = Members::findFirst(["memberid = ?0",
            "columns" =>
                "memberid,
                username,
                email,
                firstname,
                lastname,
                suffix,
                birthdate,
                gender,
                marital_status,
                fax,
                race_ethnicity,
                discipline,
                credential,
                title,
                employeetype,
                employeestatus,
                ssn,
                employeeid,
                address1,
                address2,
                city,
                state,
                zip,
                phone,
                mobile,
                remarks,
                esign_code,
                esign_code_last_updated",
                "bind" => [$memberid]
            ]);

        $member->esign_code = !empty($member->esign_code);

            $memberroles = Memberroles::memberListRoles($memberid);

            $roles = empty($memberroles) ? ["Empty roles set."] : $memberroles;


            $membersProdCred = ProdCred::membersProdCred($memberid);
            $membersHpCred = HpCred::membersHpCred($memberid);
            $membersLanguages = MLang::membersLanguages($memberid);
            if($membersLanguages){
                $member->language=$membersLanguages;
            }else{
                $member->language=[];
            }
        
            if($member){
                return ['memberdata' => $member,'roles' => $roles,'membersProdCred'=> $membersProdCred,'membersHpCred'=> $membersHpCred];
            }

            $this->ErrorMessage->thrower(
                    406,
                    "Member not found.",
                    "Member not found.",
                    "MEM002"
                );

    } //end of loadmemberbyid fucntion

    public function memberList($page=0, $count,$keyword){
        if(empty($count)){
            $count=10;
        }
        if(empty($keyword)){
            $keyword=null;
        }
        $memberresults = Members::listall($page, $count, $keyword);

        return $memberresults;

    }


    public function deleteMember($id){

        if(empty($id)){
            $this->ErrorMessage->thrower(
                    406,
                    "Empty data",
                    "Empty data",
                    "MEM002"
                );
        }

        try{
            $this->db->begin();
            $member = Members::findFirst(["memberid = ?0", "bind"=>[$id]]);

            if(!$member){
                $this->db->rollback("Cannot find user.");
            }

            if(!$member->delete()){
                $this->errors = $member->getMessage();
                $this->db->rollback("Cannot delete user.");
            }

            $deletememberrole = Memberroles::find(["memberid = ?0" , 'bind' => [$id]]);

            if ($deletememberrole !== false) {
                if ($deletememberrole->delete() === false) {
                    $this->errors = $this->errorObjectToArray($member->getMessages());
                    $this->db->rollback('Cannot delete member roles member.');
                }
            }

            $this->db->commit();

            return ["success"=>"Member has been deleted."];
        }catch(\Exception $e){
            $this->ErrorMessage->thrower(
                    406,
                    $e->getMessage(),
                    "Deleting Errors",
                    "MEM002",
                    $this->errors
                );
        }
    }

    public function memberLogin(){
        if($this->request->isPost()){
            $username   = $this->request->getPost('username');
            $password   = $this->request->getPost('password');
            $data = $this->User->authorizeUser(array("username"=>$username,"password"=>$password), "Members");
        }

        return $data;
    }

    public function refreshtoken(){

        $errorState = '';
        if($this->request->isPost()){

            $rtoken   = $this->request->getPost('refresh');

            $decoded = \Firebase\JWT\JWT::decode($rtoken, $this->config["hashkey"], array('HS256'));

            return $this->User->authorizeUser(array('memberid'=>$decoded->id),'Members', true);

        }

        $this->ErrorMessage->recNotFound("MEM-RT");

    } //end of refreshtoken fucntion

    public function passwordReset(){

        $memberid  = $this->_profileUpdate ? $this->User->getUser()->id : $this->request->getPost('memberid');

        $password   = $this->request->getPost('password');

        try{

            $member = Members::findFirst(
                        ["memberid = ?0" ,
                        'bind' => [$memberid]
                        ]);
            $this->db->begin();
            $member->password = $this->security->hash($password);

            if(!$member->save()){
                        $this->errors = $this->errorObjectToArray($member->getMessages());
                        $this->db->rollback('Cannot update password.');
                    }

            $emailcontent = [
                "username"=> $member->username,
                "password"=> $password
            ];

            $mail = $this->Mailer->sendMail($member->email, 'Medisource :Password Change Information', $emailcontent, 'passwordTemplate');

            if($mail == 0){
                $this->db->rollback("Email server does not accept your request.");
            }

            $this->db->commit();

            return array('Member Password has been updated.');

        }catch (\Exception $e) {
            $this->ErrorMessage->thrower(
                    406,
                    $e->getMessages(),
                    "Updating Password Errors",
                    "MEM002"
                );
        }
    }

    public function changePassword(){

        $password   = $this->request->getPost('password');

        if( $this->request->getPost('repassword') != $password){
            $this->ErrorMessage->thrower(
                    401,
                    "Password mismatch!",
                    "Updating Errors",
                    "MEM002"
                );
        }

        $user = Members::findFirst(["memberid = ?0",
            "bind" => [$this->User->getUser()->id]
            ]);

            if($user){
                if($this->security->checkHash($this->request->getPost('currentpassword'), $user->password)){
                    $this->_profileUpdate = true;
                    return $this->passwordReset();
                }
            }

            $this->ErrorMessage->thrower(
                    401,
                    "Your password is incorrect",
                    "Updating Errors",
                    "MEM002"
                );

    }


    public function loadprofile(){

        if(!$this->User->loggedIn()){
            $this->ErrorMessage->thrower(
                    406,
                    $e->getMessage(),
                    "Cannot load profile, you are not loggedin.",
                    "MEM002",
                    $this->errors
                );
        }

        $memberid = $this->User->getUser()->id;

        $findSq = Membersecurityquestions::findFirst(
                    [
                    "columns" => "question1, question2",
                    "memberid = ?0" ,
                        'bind' => [$memberid]
                    ]);

        $res = $this->loadmemberbyid($memberid);
        $res['security'] = $findSq;

        return $res;

    }

    public function memberuploadfile(){
        $request = new Request();
        $guid = new Guid();

        if($request->isPost()){

            $fileid             = $guid->GUID();
            $memberid           = $request->getPost('memberid');
            $filetitle          = $request->getPost('filetitle');
            $filedescription    = $request->getPost('filedescription');
            $filename           = $request->getPost('filename');
            $filetypes          = $request->getPost('filetype');
            $fileextension      = $request->getPost('fileextension');
            $filepath           = $request->getPost('path');
            $filetype           = explode("/",$filetypes);

            $checktitle = Membersfile::findFirst("filetitle = '".$filetitle."'");
            if($checktitle) {
                $data = array('conflict' => 'Something went wrong! File Title is already exist', 'statuscode' => 409 );
            }
            else {
                $savefile                   = new Membersfile();
                $savefile->fileid           = $fileid;
                $savefile->memberid         = $memberid;
                $savefile->filetitle        = $filetitle;
                $savefile->filedescription  = $filedescription;
                $savefile->filepath         = $filepath;
                $savefile->filename         = $filename;
                $savefile->filetype         = $filetype[0];
                $savefile->fileextension    = $fileextension;
                $savefile->datecreated      = date('Y-m-d H:i:s');

                if($savefile->save()){
                    $data = array('success' => 'File saved!', 'statuscode' => 200 );
                }
                else{
                    $errors = array();
                    foreach ($savefile->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    $data['err'] = $errors;
                    $data = array('error' => 'Something went wrong!', 'statuscode' => $errors );
                }
            }
        }

        return $data;

    } //end of memberuploadfile fucntion


    public function allmemberfilelist(){
        $request = new Request();

        if($request->isPost()){

            $keyword   = $request->getPost('keyword');
            $page      = $request->getPost('page');
            $sortby  = explode("/",$request->getPost('sortby'));
            $offset = ($page * 10) - 10;

            if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {

                $membersfileresults = CB::customQuery("SELECT * FROM membersfile ORDER BY ".$sortby[0]." ".$sortby[1]."  LIMIT " . $offset . ",10");

                $membersfilecount = CB::customQuery("SELECT count(fileid) as total FROM membersfile");

            }
            else{

                $membersfileresults = CB::customQuery("SELECT * FROM membersfile WHERE filetitle LIKE '%".$keyword."%' OR filedescription LIKE '%".$keyword."%' OR filename LIKE '%".$keyword."%' OR filetype LIKE '%".$keyword."%' ORDER BY ".$sortby[0]." ".$sortby[1]."  LIMIT " . $offset . ",10");

                $membersfilecount = CB::customQuery("SELECT count(fileid) as total FROM membersfile WHERE filetitle LIKE '%".$keyword."%' OR filedescription LIKE '%".$keyword."%' OR filename LIKE '%".$keyword."%' OR filetype LIKE '%".$keyword."%'");

            }

        }

        $data = array(['membersfileresults'=> $membersfileresults, 'membersfileresultscount' => $membersfilecount[0]['total'], 'page' => $page]);

        return $data;

    } //end of allmemberfilelist function


    public function memberfiledelete(){
        $request = new Request();

        if($request->isPost()){

            $fileid   = $request->getPost('fileid');
            $memberid   = $request->getPost('memberid');

            $deletefile = Membersfile::findFirst("fileid = '".$fileid."' AND memberid = '". $memberid ."'");

            if($deletefile){
                if($deletefile->delete()){
                    $data = array('success' => 'File Deleted!', 'statuscode' => 200 );
                }
                else{
                    $data = array('error' => 'Something went wrong!', 'statuscode' => 400 );
                }
            }
            else{
                $data = array('error' => 'Something went wrong!', 'statuscode' => 400 );
            }

            return $data;

        }
    } //end of memberfiledelete function


    public function memberedituploadfile(){
        $request = new Request();

        if($request->isPost()){

            $fileid             = $request->getPost('fileid');
            $memberid           = $request->getPost('memberid');
            $filetitle          = $request->getPost('filetitle');
            $filedescription    = $request->getPost('filedescription');
            $filename           = $request->getPost('filename');
            $filetypes          = $request->getPost('filetype');
            $fileextension      = $request->getPost('fileextension');
            $editmode           = $request->getPost('editmode');
            $filetype           = explode("/",$filetypes);

            $updatefile = Membersfile::findFirst("fileid = '".$fileid."' AND memberid = '". $memberid ."'");

            if($updatefile){

                if($editmode == 'withfile'){
                    $updatefile->filetitle        = $filetitle;
                    $updatefile->filedescription  = $filedescription;
                    $updatefile->filename         = $filename;
                    $updatefile->filetype         = $filetype[0];
                    $updatefile->fileextension    = $fileextension;
                }
                else{
                    $updatefile->filetitle        = $filetitle;
                    $updatefile->filedescription  = $filedescription;
                }


                if($updatefile->save()){
                    $data = array('success' => 'File updated!', 'statuscode' => 200 );
                }
                else{
                    $data = array('error' => 'Something went wrong!', 'statuscode' => 400 );
                }

            }
            else{
                $data = array('error' => $updatefile, 'statuscode' => 400 );
            }

        }

        return $data;

    } //end of memberedituploadfile function


    public function membercreatefolder(){
        $request = new Request();
        $guid = new Guid();
        $transactionManager = new TxManager();
        $transaction = $transactionManager->get();

        if($request->isPost()){

            $directoryid    = $guid->GUID();
            $memberid       = $request->getPost('memberid');
            $directorytitle = $request->getPost('directorytitle');
            $directorypath  = $request->getPost('path');
            if($directorypath == '/'){
                $contentpath = '/'.$guid->GUID();
            }
            else{
                $contentpath = $directorypath.'/'.$guid->GUID();
            }

            $findfolder = Membersdirectory::findFirst("directorytitle = '".$directorytitle."' AND directorypath = '". $directorypath ."'");
            if($findfolder){
                $data = array('error' => 'Folder is already exist!', 'statuscode' => 400 );
            }
            else{

                try {
                        $savefolder = new Membersdirectory();
                        $savefolder->setTransaction($transaction);

                        $savefolder->directoryid    = $directoryid;
                        $savefolder->memberid       = $memberid;
                        $savefolder->directorytitle = $directorytitle;
                        $savefolder->directorypath  = $directorypath;
                        $savefolder->contentpath    = $contentpath;
                        $savefolder->datecreated    = date('Y-m-d H:i:s');

                        if(!$savefolder->save()){
                            $errors = $this->geterrormessage($savefolder);
                            $transaction->rollback();
                        }

                        $transaction->commit();
                        $data = array('success' => 'New folder successfully created!', 'statuscode' => 200 );

                    }

                    catch (TxFailed $e) {
                        $data = array('error' => $errors, 'statuscode' => 400 );
                    }

            }

            return $data;

        }

    } //end of membercreatefolder function


    public function memberfilelist(){
        $request = new Request();

        if($request->isPost()){

            $keyword   = $request->getPost('keyword');
            $page      = $request->getPost('page');
            $path      = $request->getPost('path');
            $memberid  = $request->getPost('memberid');
            $sortby  = explode("/",$request->getPost('sortby'));
            $offset = ($page * 10) - 10;
            $membersfilearray = array();

            if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {

                $membersfileresults = CB::customQuery("SELECT * FROM(
                    SELECT fileid as sid, memberid as smemberid, filetitle as stitle, filedescription as sdesc, filepath as spath, 'dummy' as scontentpath,  filename as sfilename, filetype as stype, fileextension as sext, dateedited as sdateedited, datecreated as sdatecreated FROM membersfile
                    UNION
                    SELECT directoryid as sid, memberid as smemberid, directorytitle as stitle, 'dummy' as sdesc, directorypath as spath,  contentpath as scontentpath, 'dummy' as sfilename, 'folder' as stype, 'folder' as sext, dateedited as sdateedited, datecreated as sdatecreated FROM membersdirectory
                    )
                    AS searchtable WHERE smemberid = '".$memberid."' and spath = '".$path."' ORDER BY ".$sortby[0]." ".$sortby[1]." LIMIT " . $offset . ",10");

                $membersfilecount = CB::customQuery("SELECT count(sid) as total FROM(
                    SELECT fileid as sid, memberid as smemberid, filepath as spath FROM membersfile
                    UNION
                    SELECT directoryid as sid, memberid as smemberid, directorypath as spath FROM membersdirectory
                    )
                    AS searchtable WHERE smemberid = '".$memberid."' and spath = '".$path."'");

            }
            else{

                $membersfileresults = CB::customQuery("SELECT * FROM(
                    SELECT fileid as sid, memberid as smemberid, filetitle as stitle, filedescription as sdesc, filepath as spath, 'dummy' as scontentpath,  filename as sfilename, filetype as stype, fileextension as sext, dateedited as sdateedited, datecreated as sdatecreated FROM membersfile
                    UNION
                    SELECT directoryid as sid, memberid as smemberid, directorytitle as stitle, 'dummy' as sdesc, directorypath as spath,  contentpath as scontentpath, 'dummy' as sfilename, 'folder' as stype, 'folder' as sext, dateedited as sdateedited, datecreated as sdatecreated FROM membersdirectory
                    )
                    AS searchtable WHERE smemberid = '".$memberid."' AND stitle LIKE '%".$keyword."%' OR smemberid = '".$memberid."' AND stype LIKE '%".$keyword."%' ORDER BY ".$sortby[0]." ".$sortby[1]."  LIMIT " . $offset . ",10");

                $membersfilecount = CB::customQuery("SELECT count(sid) as total FROM(
                    SELECT fileid as sid, memberid as smemberid, filetitle as stitle, filetype as stype FROM membersfile
                    UNION
                    SELECT directoryid as sid, memberid as smemberid, directorytitle as stitle, 'folder' as stype FROM membersdirectory
                    )
                    AS searchtable WHERE smemberid = '".$memberid."' AND stitle LIKE '%".$keyword."%' OR smemberid = '".$memberid."' AND stype LIKE '%".$keyword."%'");

            }

        }

        $data = array('membersfileresults'=> $membersfileresults, 'membersfileresultscount' => $membersfilecount[0]['total'], 'page' => $page);
        return $data;

    } //end of memberfilelist function


    public function memberdetelefolder(){
        $request = new Request();
        $transactionManager = new TxManager();
        $transaction = $transactionManager->get();

        if($request->isPost()){

            $folderid   = $request->getPost('folderid');
            $memberid   = $request->getPost('memberid');

            try {

                $deletefolder = Membersdirectory::findFirst("directoryid = '".$folderid."' AND memberid = '". $memberid ."'");
                $deletefolder->setTransaction($transaction);
                if($deletefolder){
                    if(!$deletefolder->delete()){
                        $transaction->rollback();
                    }
                }

                foreach (Membersdirectory::find("directorypath like '%".$deletefolder->contentpath."%' AND memberid = '". $memberid ."'") as $deletefoldercontent) {
                    $deletefoldercontent->setTransaction($transaction);
                    if ($deletefoldercontent->delete() == false) {
                        $transaction->rollback();
                    }
                }


                foreach (Membersfile::find("filepath like '%".$deletefolder->contentpath."%' AND memberid = '". $memberid ."'") as $deletefilecontent) {
                    $deletefilecontent->setTransaction($transaction);
                    if ($deletefilecontent->delete() == false) {
                        $transaction->rollback();
                    }
                }


                $transaction->commit();
                $data = array('success' => 'folder successfully deleted!', 'statuscode' => 200 );

            }
            catch (TxFailed $e) {
                $data = array('error' => 'Something went wrong please try again later! Folder not deleted.', 'statuscode' => 400 );
            }

            return $data;

        }

    } //end of memberdetelefolder function


    public function editfolder(){
        $request = new Request();

        if($request->isPost()){

            $folderid   = $request->getPost('folderid');
            $foldertitle   = $request->getPost('foldertitle');
            $memberid   = $request->getPost('memberid');

            $updatefolder = Membersdirectory::findFirst("directoryid = '".$folderid."' AND memberid = '". $memberid ."'");

            if($updatefolder){
                $updatefolder->directorytitle = $foldertitle;
                if($updatefolder->save()){
                    $data = array('success' => 'folder successfully deleted!', 'statuscode' => 200 );
                }
                else{
                    $data = array('error' => 'Something went wrong please try again later! Folder not updated.', 'statuscode' => 400 );
                }
            }

            return $data;

        }
    }


    public function memberdownloadfile(){
        $request = new Request();

        if($request->isPost()){

            $memberid   = $request->getPost('memberid');
            $filename   = $request->getPost('filename');

            $key = 'uploads/memberfiles/'.$memberid.'/'.$filename;
            $presignedurl = AC::s3PreSignUrl($key, $filename, true);
            $membersfileresults['spresignurl'] = $presignedurl;

            return $presignedurl;
        }

    }

    public function saveroletemplate(){

        $guid = new Guid();

            $identifier = $guid->GUID();
            $name       = $this->request->getPost('name', ['string','trim']);
            $remarks    = $this->request->getPost('remarks', ['string','trim']);
            $roles      = $this->request->getPost('roles');

            $checkrolename = Roletemplate::findFirst(
                [
                    "name = ?0",
                    "bind" => [
                        $name,
                    ]
                ]);


            if($checkrolename){
                $this->ErrorMessage->thrower(
                    409,
                    'Role template name is already exist!',
                    'Saving Errors',
                    "MEM_ROLENAME_EXIST");
            }else{

                    try{
                        // Start a transaction
                        $this->db->begin();
                        $saveroletemplate = new Roletemplate();
                        $saveroletemplate->id = $identifier;
                        $saveroletemplate->name = $name;
                        $saveroletemplate->remarks = $remarks;
                        if(!$saveroletemplate->save()){
                            $this->errors = $this->errorObjectToArray($saveroletemplate->getMessages());
                            $this->db->rollback("savig here");
                        }

                        $res = Roletemplate::insert($roles, $identifier);

                        if(is_object($res)){
                            $this->errors = $res->getMessage();
                            $this->db->rollback();
                        }

                        $this->db->commit();
                    }catch(\Exception $e){
                        $this->ErrorMessage->thrower(
                            406,
                                "rollbaclk error",
                                "rollbaclk error",
                                "MEM_ROLE_SAVING",
                                $this->errors
                            );
                    }
             }
             return array('success' => 'Role template successfully saved!', 'id'=> $identifier);
    }

    public function updateroletemplate(){

            $identifier = $this->request->getPut('id', ['trim']);
            $name       = $this->request->getPut('name', ['string','trim']);
            $remarks    = $this->request->getPut('remarks', ['string','trim']);
            $roles      = $this->request->getPut('roles');

            $checkrolename = Roletemplate::findFirst(
                [
                    "name = ?0 AND id != ?1",
                    "bind" => [
                        $name,
                        $identifier
                    ]
                ]);

            if($checkrolename){
                $this->ErrorMessage->thrower(
                    409,
                    'Role template name is already exist!',
                    'Saving Errors',
                    "MEM_ROLENAME_EXIST");
            }else{

                    try{
                        // Start a transaction
                        $this->db->begin();
                        $saveroletemplate = Roletemplate::findFirst(
                        [
                            "id = ?0",
                            "bind" => [
                                $identifier
                            ]
                        ]);

                        if(!$saveroletemplate){
                            $this->ErrorMessage->thrower(
                                406,
                                "Current Role is missing.",
                                "Updating Role Error",
                                "MEM002"
                            );
                        }

                        $saveroletemplate->name = $name;
                        $saveroletemplate->remarks = $remarks;
                        if(!$saveroletemplate->save()){
                            $this->errors = $this->errorObjectToArray($saveroletemplate->getMessages());
                            $this->db->rollback("update error");
                        }

                        $templateitems = Roletemplateitems::find(["roletemplateid = ?0" , 'bind' => [$identifier]]);

                        if ($templateitems !== false) {
                            if ($templateitems->delete() === false) {
                                $this->errors = $this->errorObjectToArray($templateitems->getMessages());
                                $this->db->rollback('Cannot delete roles items.');
                            }
                        }

                        $res = Roletemplate::insert($roles, $identifier);

                        if(is_object($res)){
                            $this->errors = $res->getMessage();
                            $this->db->rollback();
                        }

                        $this->db->commit();
                    }catch(\Exception $e){
                        $this->ErrorMessage->thrower(
                            406,
                                "rollbaclk error",
                                "rollbaclk error",
                                "MEM_ROLE_UPDATING",
                                $this->errors
                            );
                    }
             }
             return array('success' => 'Role template successfully updated!', 'id'=> $identifier);

    }

    public function getroletemplate(){
        $rolesTemplate = Roletemplate::find();

        if($rolesTemplate){
            return $rolesTemplate->toArray();
        }

        return ['success' => 'Empty Set.', 'ResponseCode' => 206 ];
    }

    public function loadroletemplatebyid($id){
        $request = new Request();
        if(!empty($id)){

            $rolesTemplate = Roletemplate::findFirst(
                [
                    "id = ?0",
                    "bind" => [
                        $id,
                    ]
                ]);

            if($rolesTemplate){
                $data = $rolesTemplate->getRoletemplateitems(["columns" => "role, roleGroup , roletemplateid"]);
                $newData = [];
                foreach($data as $rti){
                    $newData[$rti['roleGroup']][] = $rti['role'];
                }
                return ['roles'=>$newData, 'info'=> $rolesTemplate->toArray()];
            }

            return ['success' => 'Empty Set.', 'ResponseCode' => 206 ];
        }

        $this->ErrorMessage->thrower(
                    406,
                    $e->getMessage(),
                    "Deleting Errors",
                    "MEM002",
                    $this->errors
                );

    }

    public function roletemplatelist($page=0, $count,$keyword){

        if(empty($count)){
            $count=10;
        }
        if(empty($keyword)){
            $keyword=null;
        }
        $templates = Roletemplate::listall($page, $count, $keyword);

        return $templates;

    } //end of roletemplatelist function

    public function deleteroletemplate($id){
        if(empty($id)){
            $this->ErrorMessage->thrower(
                    406,
                    "Empty ID",
                    "Empty ID",
                    "MEM002"
                );
        }

        try{
            $this->db->begin();
            $template = Roletemplate::findFirst(["id = ?0", "bind"=>[$id]]);

            if(!$template){
                $this->db->rollback("Cannot find template.");
            }

            if(!$template->delete()){
                $this->errors = $template->getMessage();
                $this->db->rollback("Cannot delete template.");
            }

            $templateitems = Roletemplateitems::find(["roletemplateid = ?0" , 'bind' => [$id]]);

            if ($templateitems !== false) {
                if ($templateitems->delete() === false) {
                    $this->errors = $this->errorObjectToArray($templateitems->getMessages());
                    $this->db->rollback('Cannot delete member roles member.');
                }
            }

            $this->db->commit();

            return ["success"=>"Template has been deleted."];
        }catch(\Exception $e){
            $this->ErrorMessage->thrower(
                    406,
                    $e->getMessage(),
                    "Deleting Errors",
                    "MEM002",
                    $this->errors
                );
        }
    }

    public function updatepasswordfirstlogin(){
        $request = new Request();
        $transactionManager = new TxManager();
        $transaction = $transactionManager->get();

        if($request->isPost()){

            $memberid = $request->getPost('memberid');
            $password = $request->getPost('password');
            $skip = $request->getPost('skip');

            try {

                if($skip == 'false'){

                    $authenticatemember = Members::findFirst("memberid='".$memberid."'");
                    $authenticatemember->setTransaction($transaction);
                    $loginstatus = $authenticatemember->getlogininfo();

                    if($authenticatemember && $loginstatus->status == 'first'){

                        $authenticatemember->password = sha1($password);
                        if(!$authenticatemember->save()){
                            $transaction->rollback();
                        }

                    }
                    else{
                        $transaction->rollback();
                    }
                }

                $updateloginstatus = Logininfo::findFirst("memberid='".$memberid."'");
                $updateloginstatus->setTransaction($transaction);
                if($updateloginstatus){
                    $updateloginstatus->status = 'security';
                    if(!$updateloginstatus->save()){
                        $transaction->rollback();
                    }
                }
                else{
                    $transaction->rollback();
                }


                $transaction->commit();
                $data = array('success' => 'Your password is successfully updated!', 'statuscode' => 200 );

            }
            catch (TxFailed $e) {
                $data = array('error' => 'Something went wrong please try again later!', 'statuscode' => 400 );
            }



        }
        return $data;
    }

    public function getMemberList(){
        $data = CB::customQuery("SELECT * FROM members ORDER BY firstname, lastname");
        $data = (count($data)!=0 ? array('success' => 'Fetching members record','data' => $data, 'statuscode' => 200 ) : array('error' => 'Something went wrong please unable to fetch mamber record try again later!', 'data'=>$data, 'statuscode' => 400 ));
        return $data;
    }


    public function updateprofile(){

        if($this->User->getUser()->id != $this->request->getPut('memberid')){
                $this->ErrorMessage->thrower(
                    406,
                    "This seem to be a mistake, your not the person your are updating.",
                    "Updating Errors",
                    "MEM002"
                );
        }


        $user = Members::findFirst(["memberid = ?0",
            "bind" => [$this->User->getUser()->id]
            ]);


        if($user){
            if($this->security->checkHash($this->request->getPut('password'), $user->password)){
                //return $this->editMember();

                parse_str(file_get_contents("php://input"), $put);

                if(empty($put['memberid'])){
                    $this->ErrorMessage->thrower(
                            406,
                            "Current User is missing.",
                            "Updating Error",
                            "MEM002"
                        );
                }

                $this->_member['memberid'] = $this->User->getUser()->id;
                $this->_member['tab'] = $this->request->getPut('tab',['trim']);

                if($this->_member['tab'] == "account" ){
                    $this->validationResponseError(new CMValidator(), $put,
                    [
                        "username" => new Uniquememberdata([
                                'message' => 'Username already taken by another user.',
                                'model' => '\Models\Members',
                                'exclude' => [
                                    'con' => ' memberid != ?1 ',
                                    'bind' => [$this->request->getPut('memberid',['trim'])]
                                ]
                        ]),
                        "email" => new Uniquememberdata([
                                'message' => 'Email already taken by another user.',
                                'model' => '\Models\Members',
                                'exclude' => [
                                    'con' => ' memberid != ?1 ',
                                    'bind' => [$this->request->getPut('memberid',['trim'])]
                                ]
                        ])
                        // SSN ADD UNIQUE
                    ]);
                    $this->_member['username']      = $this->request->getPut('username',['alphanum','trim']);
                    $this->_member['email']         = $this->request->getPut('email', ['email','trim']);
                    $this->_member['firstname']     = $this->request->getPut('firstname', ['string','trim']);
                    $this->_member['lastname']      = $this->request->getPut('lastname', ['string','trim']);
                    $this->_member['suffix']        = $this->request->getPut('suffix', ['string','trim']);
                    $this->_member['birthdate']     = $this->request->getPut('birthdate');
                    $this->_member['gender']        = $this->request->getPut('gender', ['string','trim']);
                    $this->_member['credential']    = $this->request->getPut('credential', ['string','trim']);
                    $this->_member['title']         = $this->request->getPut('title', ['string','trim']);
                }

                if($this->_member['tab'] == "basic"){
                    $this->_member['ssn']           = $this->request->getPut('ssn', ['string','trim']);
                    $this->_member['employeeid']    = $this->request->getPut('employeeid', ['string','trim']);
                    $this->_member['address1']      = $this->request->getPut('address1', ['string','trim']);
                    $this->_member['address2']      = $this->request->getPut('address2', ['string','trim']);
                    $this->_member['city']          = $this->request->getPut('city', ['string','trim']);
                    $this->_member['state']         = $this->request->getPut('state', ['string','trim']);
                    $this->_member['zip']           = $this->request->getPut('zip', ['string','trim']);
                    $this->_member['phone']         = $this->request->getPut('phone', ['string','trim']);
                    $this->_member['mobile']        = $this->request->getPut('mobile', ['string','trim']);
                }



                try {

                    $this->db->begin();

                    if($this->_member['tab'] == "security"){
                        $findSq = Membersecurityquestions::findFirst(
                            ["memberid = ?0" ,
                                'bind' => [$this->_member['memberid']]
                            ]);

                        $sq = (!$findSq ? new Membersecurityquestions() : $findSq);

                        $sq->memberid = $this->User->getUser()->id;
                        $sq->question1 = $this->request->getPut('sc1', ['string','trim']);
                        $sq->answer1 = $this->request->getPut('answer1', ['string','trim']);
                        $sq->question2 = $this->request->getPut('sc2', ['string','trim']);
                        $sq->answer2 = $this->request->getPut('answer2', ['string','trim']);

                        if(empty($this->request->getPut('sc1')) || empty($this->request->getPut('answer1')) ||
                            empty($this->request->getPut('sc2')) || empty($this->request->getPut('answer2'))){
                            $this->ErrorMessage->thrower(
                                406,
                                "All fields are required.",
                                "Updating Error",
                                "MEM002"
                            );
                        }

                        if(!$sq->save()){
                            $this->ErrorMessage->thrower(
                                406,
                                "Cannot save security questions.",
                                "Updating Error",
                                "MEM002"
                            );
                        }
                        return array('Member security questions have been saved.');
                    }

                    $member = Members::findFirst(
                        ["memberid = ?0" ,
                        'bind' => [$this->_member['memberid']]
                        ]);

                    if(!$member){
                        $this->ErrorMessage->thrower(
                            406,
                            "Current User is missing.",
                            "Updating Error",
                            "MEM002"
                        );
                    }

                    if(!$member->save($this->_member)){
                        $this->errors = $this->errorObjectToArray($member->getMessages());
                        $this->db->rollback('Cannot update member.');
                    }

                    $this->db->commit();

                    return array('Member have been saved.');
                }
                catch (\Exception $e) {
                    $this->ErrorMessage->thrower(
                        406,
                        $e->getMessage(),
                        "Updating Errors",
                        "MEM002",
                        $this->errors
                    );
                }

            }
        }
                $this->ErrorMessage->thrower(
                    401,
                    "Your password is incorrect",
                    "Updating Errors",
                    "MEM002"
                );
    }

    public function esignUpdate(){

        parse_str(file_get_contents("php://input"), $put);

        $this->validationResponseError(new Esign(), $put);

        if( $this->request->getPut('esign') != $this->request->getPut('reesign')){
            $this->ErrorMessage->thrower(
                    406,
                    "E-sign code mismatch!",
                    "Updating Errors",
                    "MEM002"
                );
        }

        try{
            $this->db->begin();
            $user = Members::findFirst(["memberid = ?0",
                "bind" => [$this->User->getUser()->id]
                ]);

            if(!$user){
                    $this->ErrorMessage->thrower(
                        409,
                        "Your password is incorrect",
                        "Updating Errors",
                        "MEM002"
                    );
            }

            if(!$this->security->checkHash($this->request->getPut('password'), $user->password)){
                    $this->ErrorMessage->thrower(
                        409,
                        "Your password is incorrect",
                        "Updating Errors",
                        "MEM002"
                    );
            }

            $user->esign_code = $this->security->hash($this->request->getPut('esign'));
            $user->esign_code_last_updated = date("Y-m-d H:i:s");

            if(!$user->save()){
                $this->errors = $this->errorObjectToArray($user->getMessages());
                $this->db->rollback('Cannot update esign.');
            }

                //save data to database if there is no error
                $this->db->commit();

                //success message
                return array('Success');

        }catch(\Exception $e){
            $this->ErrorMessage->thrower(
                    401,
                    $e->getMessages(),
                    "Updating Errors",
                    "MEM002"
                );
        }

    }

    public function setSecurity(){

        $this->validationResponseError(new SS(), $_POST);

            try {

                $this->db->begin();

                $memberid = $this->User->getUser()->id;
                $password = $this->request->getPost("password", ['trim']);
                $repassword = $this->request->getPost("repassword", ['trim']);
                $currentpassword = $this->request->getPost("currentpassword", ['trim']);

                if( $repassword != $password){
                    $this->ErrorMessage->thrower(
                            401,
                            "Password mismatch!",
                            "Updating Errors",
                            "MEM002"
                        );
                }

                if( $currentpassword == $password){
                    $this->ErrorMessage->thrower(
                            401,
                            "Do not use your temporary password as your new password!",
                            "Updating Errors",
                            "MEM002"
                        );
                }

                $user = Members::findFirst(["memberid = ?0",
                    "bind" => [$memberid]
                    ]);

                if(!$user){
                    $this->ErrorMessage->thrower(
                            401,
                            "Your current password is incorrect.",
                            "Updating Errors",
                            "MEM002"
                        );
                }

                if(!$this->security->checkHash( $currentpassword, $user->password)){
                    $this->ErrorMessage->thrower(
                            401,
                            "Your current password is incorrect.",
                            "Updating Errors",
                            "MEM002"
                        );
                }

                $member = Members::findFirst(
                        ["memberid = ?0" ,
                        'bind' => [$memberid]
                        ]);

                $member->password = $this->security->hash($password);
                $member->account_security = 1;

                if(!$member->save()){
                    $this->errors = $this->errorObjectToArray($member->getMessages());
                    $this->db->rollback('Cannot update password.');
                }

                $findSq = Membersecurityquestions::findFirst(
                    ["memberid = ?0" ,
                        'bind' => [$memberid]
                    ]);

                $sq = (!$findSq ? new Membersecurityquestions() : $findSq);

                $sq->memberid = $this->User->getUser()->id;
                $sq->question1 = $this->request->getPost("sc1");
                $sq->answer1 = $this->security->hash($this->request->getPost("answer1"));
                $sq->question2 = $this->request->getPost("sc2");
                $sq->answer2 = $this->security->hash($this->request->getPost("answer2"));

                if(!$sq->save()){
                    $this->errors = $this->errorObjectToArray($sq->getMessages());
                    $this->db->rollback('Cannot save security questions.');
                }

                //save data to database if there is no error
                $this->db->commit();

                $emailcontent = ["username"=> $member->username];

                $mail = $this->Mailer->sendMail($member->email, 'Medisource :Account Security Information', $emailcontent, 'securityEmailTemplate');

                //success message
                return array('Member Account Security has been updated.');
            }
            catch (\Exception $e) {
                $this->ErrorMessage->thrower(
                    406,
                    $e->getMessage(),
                    "Updating Account Security Errors",
                    "MEM002",
                    $this->errors
                );
            }

        // if($request->isPost()){

        //     $memberid  = $request->getPost('memberid');
        //     $question1  = $request->getPost('sc1');
        //     $answer1   = $request->getPost('sca1');
        //     $question2  = $request->getPost('sc2');
        //     $answer2   = $request->getPost('sca2');

        //     $one = ['question' => $question1, 'answer' => $answer1];
        //     $two = ['question' => $question2, 'answer' => $answer2];

        //     $securityquestions = [];

        //     array_push($securityquestions, $one);
        //     array_push($securityquestions, $two);



        //     try {

        //         $deletequestions = Membersecurityquestions::find("memberid = '".$memberid."'");
        //         // $deletequestions->setTransaction($transaction);

        //         if($deletequestions){
        //             if(!$deletequestions->delete()){
        //                 $transaction->rollback();
        //             }
        //         }


        //         foreach ($securityquestions as $list) {
        //             $savequestion = new Membersecurityquestions();
        //             $savequestion->memberid = $memberid;
        //             $savequestion->question = $list['question'];
        //             $savequestion->answer = $list['answer'];

        //             if(!$savequestion->save()){
        //                 $errors = CB::geterrormessage($savequestion);
        //                 $transaction->rollback();
        //             }

        //             // $datadata[] = $securityquestions;
        //         }

        //         $updateloginstatus = Logininfo::findFirst("memberid='".$memberid."'");
        //         $updateloginstatus->setTransaction($transaction);
        //         if($updateloginstatus){
        //             $updateloginstatus->status = 'active';
        //             if(!$updateloginstatus->save()){
        //                 $transaction->rollback();
        //             }
        //         }
        //         else{
        //             $transaction->rollback();
        //         }

        //         $transaction->commit();
        //         $data = array('success' => 'Your Security question is successfully saved!', 'statuscode' => 200 );


        //     }
        //     catch (TxFailed $e) {
        //         $data = array('error' => 'Something went wrong please try again later!', 'statuscode' => 400 );
        //     }

        //     return $data;

        // }

    }

     public function loginstatus($memberid){

        $checkmember = Members::findFirst("memberid='".$memberid."'");

        if($checkmember){

            $loginstatus = $checkmember->getlogininfo();

            $data = array('success' => $loginstatus->status, 'statuscode' => 200 );
        }

        return $data;

    } //end of checktoken fucntion

    public function getsecurityquestion(){
        $getquestion = Securityquestions::find();

        if($getquestion){
            return $getquestion->toArray();
        }

        return ["empty set"];
    }

    public function getRoleitems(){

        $roles = [];
        $roleitem = Roleitems::find();
        $rolegroups = Rolegroups::find(['columns' => 'roleGroup, title']);

        if($roleitem){
            foreach ($roleitem as $list) {
                if($list->roleGroup == 'sys'){
                    if(!in_array($list->roleCode, RolesConstants::DEFAULTROLES)){
                         $roles[$list->roleGroup][] = ['id' => $list->roleCode, 'text' => $list->roleName];
                    }
                }else{
                    $roles[$list->roleGroup][] = ['id' => $list->roleCode, 'text' => $list->roleName];
                }
            }
            return [$roles, $rolegroups->toArray()];
        }

        $this->ErrorMessage->thrower(
                    406,
                    "No role items found",
                    "Error roles items",
                    "MEM002"
                );
    }

    public function getRoleitemsCb(){
        return ['roleitems' => $this->getRoleitems()[0], 'rolegroups' => $this->getRoleitems()[1]];
    }

    public function generateUserCacheAcl($memberid){
        $collections = $this->collections->getCollections();
        return AclRoles::singleUserAcl($memberid, $collections, '\Models\Memberroles');
    }

    // public function initializeRoles(){

    //     $rec  = Memberroles::find();
    //     $own = Members::findFirst(
    //         array(
    //             "employeetype = 'Owner'",
    //             "columns" => "memberid"
    //         ));

    //     $rolesResources = array($own->memberid => array());
    //     $collections = $this->collections->getCollections();

    //     foreach($collections as $col){
    //         foreach($col->getHandlers() as $colh) {
    //             $controller = str_replace('Controllers\\','',$col->getHandler());
    //             $rolesResources[$own->memberid][$controller][] = $colh[2];
    //             foreach($rec as $r){
    //                 if($colh[3] == $r->role) {
    //                     $rolesResources[$r->memberid][$controller][] = $colh[2];
    //                 }
    //             }
    //         }
    //     }

    //     $msg = array();

    //     foreach ($rolesResources as $key => $value) {
    //         $msg[$key] = AclRoles::setAcl([$key => $value],[$key],  $key."-ACL_RECORD") ? "Successfully created your roles!": "Unsuccessfull";
    //     }

    //     return $msg;

    // }

    public function fpCheckIdentity(){

        $this->validationResponseError(new Username(), $_POST);
        $this->validationResponseError(new UserEmail(), $_POST);

        $username = $this->request->getPost("username", ["trim"]);
        $email = $this->request->getPost("email", ["trim"]);

        $member = Members::findFirst(
            ["username = ?0 AND email = ?1" ,
            'bind' => [$username, $email]
            ]);

        if(!$member){
            $this->ErrorMessage->thrower(
                401,
                "We cannot find your identity. Your username and email should match.",
                "Updating Errors",
                "MEM002"
            );
        }

        if($member->account_security == 0){
            $this->ErrorMessage->thrower(
                401,
                "Please contact your administrator for your account credentials, your account have not yet completed the basic security requirement.",
                "Updating Errors",
                "MEM002"
            );
        }

        $this->wap->firewallCheck($member->memberid);

        $guid = new Guid();
        $time = time();
        $exp = strtotime($this->config['tokenEXP']['fpToken'], $time);
        try{

            $this->db->begin();

            // Replacing existing request but still storing them.
            $fpsearch = ForgotPassword::findFirst(
                    ["userid = ?0 AND step != 'SUCCESS'" ,
                    'bind' => [$member->memberid]
                ]);


            $fp = (!$fpsearch ? new ForgotPassword() : $fpsearch);

            $fp->guid = $guid->GUID();
            $fp->userid = $member->memberid;
            $fp->fp_expiration = date('Y-m-d H:i:s', $exp);
            $fp->step = 'IDENTITY';

            if(!$fp->save()){
                $this->errors = $this->errorObjectToArray($fp->getMessages());
                $this->db->rollback('Cannot process forgot password.');
            }

            $payload = [
                "id" => $fp->guid,
                "exp" => $exp,
                "iat" => $time
            ];

            $this->db->commit();

            return $this->fpAccessToken($payload);

        }catch(\Exception $e) {

            $this->ErrorMessage->thrower(
                406,
                $e->getMessage(),
                "Forgot Password Error!",
                "MEM002",
                $this->errors
            );
        }
    }

    public function fpResetRequest(){
        //Get Answers and validate them
        //Create a function to block requests attempts, create a table to put all blocked users
        //Create Event Controller to check for user/members blocked, let them redirect to login or password reset with a message attempts reached wait for another 1 hour
        //If request valid send an email with an 8 character code
        $this->validationResponseError(new RequestResetPW(), $_POST);
        $token = $this->wap->tokenChecker();
        $ans = Securityquestions::fpGet($token->id, ["question1", "question2", "answer1", "answer2", "members.email","members.memberid"]);

        if(empty($ans)){
            //Return Error
            throw new \Micro\Exceptions\HTTPExceptions(
                "Your password request is already invalid!",
                406,
                array(
                    'dev' => "adsfadsfadsf",
                    'internalCode' => "EXC001"
                )
            );
        }

        $this->wap->firewallCheck($ans[0]['memberid']);

        $email = $ans[0]["email"];
        $memberid = $ans[0]["memberid"];
        // unset($ans[0]["email"]);
        // unset($ans[0]["memberid"]);

        if(
            $ans[0]['question1'] !== $this->request->getPost('question1') ||
            $ans[0]['question2'] !== $this->request->getPost('question2') ||
            !$this->security->checkHash($this->request->getPost('answer1'), $ans[0]['answer1']) ||
            !$this->security->checkHash($this->request->getPost('answer2'), $ans[0]['answer2'])
            )
        {
            $this->attempts('add', $token->id);
            //Return Error
            throw new \Micro\Exceptions\HTTPExceptions(
                "Your answers are incorrect!",
                406,
                array(
                    'dev' => "adsfadsfadsf",
                    'internalCode' => "EXC001",
                    'more' => ["attempts"=>($this->config['attempts']['fp'] - $this->attempt)],
                )
            );
        }

        //Send email with 8 code
        $code = CB::randomPassword();

        //Close close Member Account
        $mem = Members::findFirst(["memberid=?0", "bind"=> [$memberid]]);
        $mem->employeestatus = "INACTIVE";
        $mem->save();

        if(!$mem->save()){
            $this->ErrorMessage->thrower(
                500,
                "Cannot deactivate User!",
                "Forgot Password Error!",
                "MEM002"
            );
        }

        $mail = $this->Mailer->sendMail($email, 'Medisource :Forgot Password Request PIN', $code, 'fpPINTemplate');
        $this->attempts('reset', $token->id, $code);

        return ["Success!, you may check your email for your pin for reset password."];
    }

    public function fpPasswordReset(){
        // Check Fields if available
        // Check for Token
        // Check for if new password and confirm is the same
        // Check for PIN if correct, store attempts
        // Change the existing password with the new one, remove the forgotpassword table record
        // Email about the successfull change of password

        $this->validationResponseError(new NewPasswordReset(), $_POST);
        $token = $this->wap->tokenChecker();
        $ans = Securityquestions::fpGet($token->id, ["members.memberid","fp_code", "fp_code_expiration"], 'PENDING');

        if(empty($ans[0])){
            $this->ErrorMessage->thrower(
                    406,
                    "Password Reset Request has already expired.",
                    "Forgot Passowrd Error!",
                    "MEM002"
                );
        }

        $this->wap->firewallCheck($ans[0]['memberid']);

        $memberid = $ans[0]['memberid'];

        if(strtotime($ans[0]['fp_code_expiration']) < time()){
            $this->errors[] = 'Pin Code has expired';
        }

        if($ans[0]['fp_code'] != $this->request->getPost('pin')){
            $this->errors[] = 'Invalid Pin.';
        }

        if($this->request->getPost('newpass') != $this->request->getPost('confirmpass')){
            $this->errors[] = 'Password does not match.';
        }

        if(!empty($this->errors)){
            $this->attempts('add', $token->id);
            $this->ErrorMessage->thrower(
                    406,
                    "Something went wrong.",
                    "Forgot Passowrd Error!",
                    "MEM002",
                    [
                        $this->errors,
                        "attempts" => ($this->config['attempts']['fp'] - $this->attempt)
                    ]
                );
        }

        //Activate close Member Account
        $mem = Members::findFirst(["memberid=?0", "bind"=> [$memberid]]);
        $mem->employeestatus = "ACTIVE";
        $mem->password = $this->security->hash($this->request->getPost('newpass'));

        if(!$mem->save()){
            $this->ErrorMessage->thrower(
                500,
                "Cannot activate User!",
                "Forgot Password Error!",
                "MEM002"
            );
        }

        $sp = Forgotpassword::findFirst(["guid = ?0", "bind" => [$token->id]]);
        $sp->step = 'SUCCESS';
        if(!$sp->save()){
            $this->ErrorMessage->thrower(
                500,
                "Cannot update row",
                "Forgot Password Error!",
                "MEM002"
            );
        }

        $mail = $this->Mailer->sendMail($mem->email, 'Medisource :Forgot Password Request Success', $mem->username , 'fpPasswordChange');

        return ["Your password has been changed!"];
    }

    private function fpAccessToken($payload){
        $accesstoken = \Firebase\JWT\JWT::encode($payload, $this->config['hashkey']);
        return array(
            'fp_token' => $accesstoken
        );
    }

    public function fpGetQuestions(){
        $payload = $this->wap->tokenChecker();

        $data = Securityquestions::fpGet($payload->id);

        $this->wap->firewallCheck($data[0]['memberid']);

        if(strtotime($data[0]['fp_expiration']) < time() || empty($data[0])){
            $this->ErrorMessage->thrower(
                401,
                "This password reset request has been expired.",
                "Forgot Password Error!",
                "MEM002"
            );
        }

        return $data;
    }

    private function attempts($post="add", $id, $code=null){

        $sp = Forgotpassword::findFirst(["guid = ?0", "bind" => [$id]]);

        $this->attempt = 0;

        switch ($post) {
            case 'add':
                $sp->attempts++;
                $this->attempt = ($sp->attempts - 1);

                if($sp->attempts >= $this->config['attempts']['fp']){

                    $sp->attempts = 0;
                    $sp->save();

                    $this->wap->addToUserBlacklist([
                        "id" => $sp->userid,
                        "reason" => "Attempting to reset password."
                        ]);

                    return true;
                }

                $sp->save();
                break;
            case 'reset':
                $sp->attempts = 0;
                $sp->fp_code = $code;
                $sp->step = "PENDING";
                $sp->fp_code_expiration = date('Y-m-d H:i:s', strtotime('+60 minutes', time()));
                $sp->save();
                break;
            case 'get':
                return $sp->attempts;
                break;
        }

        if(!$sp->save()){
            $this->ErrorMessage->thrower(
                500,
                "Cannot update attempts",
                "Forgot Password Error!",
                "MEM002"
            );
        }
    }

    public function loadMemberByTitle() {
        $data = array();
        $title = $this->request->getPost('title', ['string','striptags', 'trim']);
        if($title) {
            $data = Members::find([
                "columns" => "CONCAT(firstname, ' ', lastname) AS name, memberid",
                "title = :title:",
                "bind" => [
                    "title" => $title
                ],
                "order" => "name"
            ])->toArray();
        }

        return $data;
    }

    public function set_document_esign()
    {
        $code           = $this->request->getPost('code', ['string','striptags', 'trim']);
        $documentid     = $this->request->getPost('id', ['string','striptags', 'trim']);
        $documenttype   = $this->request->getPost('type', ['string','striptags', 'trim']);

        if(!empty($code)) {
            $member = Members::findFirst([
                "columns" => "CONCAT(firstname,lastname,',',credential) AS name, CONCAT(firstname,' ',lastname) AS fullname, memberid, esign_code",
                "memberid = :memberid:",
                "bind" => [
                    "memberid" => $this->User->getUser()->id
                ]
            ]);

            if($member) {
                if($this->security->checkHash($code, $member->esign_code)) {
                    $documenttype = strtolower(str_replace("ORDERS-", "", $documenttype));
                    $model = OrderConstants::MODELS[$documenttype];
                    $model = $model::findFirst([
                        "orderidfk = :orderidfk:",
                        "bind" => [
                            "orderidfk" => $documentid
                        ]
                    ]);

                    if($model) {
                        $model->signature_memberid = $this->User->getUser()->id;
                        if(!$model->save()) {
                            $this->errors = "Unable to set e-signature";
                        }
                    }

                    return $member;
                }
            }

            $this->errors = "Invalid e-signature code";
        } else {
            $this->errors = "E-signature code is required";
        }

        $this->ErrorMessage->thrower(
            406,
            "Something went wrong, please try again later.",
            "Fetching Errors",
            "MDP020",
            $this->errors
        );
    }

    public function get_members_selection() {
        $request = new Request();
        die(var_dump($request->getQuery()));
    }
 

    public function getMembersByStatus($status) {

        $status = strtoupper($status);

        $columns = [
            "memberid",
            "CONCAT(firstname, ' ', lastname, ', ', credential) AS name"
        ];

        $query = " SELECT " . implode(',', $columns) . " FROM members ";

        if ($status != "ALL") {
            $query .= " WHERE employeestatus = '" . $status . "' ";
        }

        $query .= " ORDER BY firstname, lastname";

        return (new CB)->customQuery($query);

    }
}
