<?php

namespace Controllers;

use \Controllers\ControllerBase as CB;

class AuthenticateController extends \Phalcon\Mvc\Controller {


    /**
     * @SWG\Get(
     *     path="/authenticate/get",
     *     summary="Get Authentication",
     *     tags={"Authentication"},
     *     description="Description all",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Check if your bearer token is still valid",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation"
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     )
     * )
     *
     */

    public function getAccessTokenAction() {


        $request = new \Phalcon\Http\Request();

        $jwt = new \Security\Jwt\JWT();

        $app = new CB;

        $parsetoken = explode(" ",$request->getHeader('Authorization'));

        $token = $jwt->decode($parsetoken[1], $app->getConfig()['hashkey'], array('HS256'));


    }

    public function skipAction($name) {
        echo "auth skipped ($name)";
    }
}
