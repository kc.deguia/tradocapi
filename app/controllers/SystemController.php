<?php

namespace Controllers;


use Models\Members;
use Security\AclRoles;

class SystemController extends ControllerBase {


    /**
     * @SWG\Get(
     *     path="/authenticate/get",
     *     summary="Get Authentication",
     *     tags={"Authentication"},
     *     description="Description all",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Check if your bearer token is still valid",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation"
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     )
     * )
     *
     */
    
    protected $userRoles;

    public function initializeRoles(){

        $collections = $this->collections->getCollections();

        $member = Members::find();

        return AclRoles::sourceAcl($member, $collections, '\Models\Memberroles');

    }

    public function generateUserCacheAcl($memberid){
        $collections = $this->collections->getCollections();        
        return AclRoles::singleUserAcl($memberid, $collections, '\Models\Memberroles');
    }

    private function getNav($nav){
        $userNav = [];
        $user = $this->userRoles;
        //Checking for User mcrypt_module_close(td)
        foreach($nav as $k => $v){
            if(isset($v['roles_group'])){
                if(isset($user[$v['roles_group']]) || $user == 'Owner'){
                    if($user!='Owner'){
                        $arr = array_intersect( $user[$v['roles_group']] , $v['roles']);    
                    }else{
                        $arr = ['Owner'];
                    }

                    if(!empty($arr) ){
                        $userNav[$k] = [
                                'title' => $v['title'],
                                'url' => $v['url'],
                                'icon' => isset($v['icon']) ? $v['icon'] : ''
                            ];
                        if(isset($v['sub'])){         
                            $userNav[$k]['sub'] = $this->getNav($v['sub']);
                        }
                    }
                }
            }else{
                $userNav[$k] = [
                    'title' => $v['title'],
                    'url' => $v['url'],
                    'icon' => isset($v['icon']) ? $v['icon'] : ''
                ];
            }
        }

        return $userNav;
    }

    public function navigation(){
        $r = new \ReflectionClass('\Constants\NavConstants');
        $nav = $r->getConstant('SIDEBARNAV');

        // $this->userRoles = $this->User->getRoles();

        $user = $this->User->getUserComplete();

        $this->userRoles = ($user['employeetype'] == 'Owner' ? $user['employeetype'] : $user['roles']);

        $userNav = $this->getNav($nav);
        
        return $userNav;

    }

    public function test1(){
        return ["test1"];
    }

    public function test2(){
        return ["test2"];   
    }

    public function test3(){
        return ["test3"];   
    }

    public function test4(){
        return ["test4"];   
    }

    public function testpmap(){
        return ["testpmap"];   
    }

    public function testnew(){
        return ["testnew"];   
    }

    public function bagongroute(){
        return ["bagongroute"];   
    }
}
