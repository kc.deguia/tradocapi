<?php

namespace Constants;

class SpecialProceduresConstants
{

    CONST OASISCATHTYPEVAL = [
        'intermittent'  => 'int-self',
        'external'      => 'int-irrigation'
    ];
    
    CONST CATHETERTYPE = [ 
        'indwelling'        => 'Indwelling Catheter',
        'suprapubic'        => 'Suprapubic Catheter',
        'int-self'          => 'Intermittent Self-Catheterization',
        'int-irrigation'    => 'Intermittent Irrigation',
        'urethral'          => 'Urethral'
    ];

    CONST VENOUSACCESSTYPE = [
        'peripheral-iv'         => 'Peripheral IV',
        'picc-line'             => 'PICC Line',
        'cvad'                  => 'CVAD'
    ];

    CONST MODELS = [
        'indwelling'            => 'Models\Sp_catheter',
        'suprapubic'            => 'Models\Sp_catheter',
        'int-self'              => 'Models\Sp_catheter',
        'int-irrigation'        => 'Models\Sp_catheter',
        'urethral'              => 'Models\Sp_catheter',
        'enteral-feeding'       => 'Models\Sp_enteral_feeding',
        'ostomy'                => 'Models\Sp_ostomy',
        'peritoneal-dialysis'   => 'Models\Sp_peritoneal_dialysis',
        'ventilator'            => 'Models\Sp_ventilator',
        'pacemaker'             => 'Models\Sp_pacemaker',
        'pacemaker-icd'         => 'Models\Sp_pacemaker_icd_icm',
        'pacemaker-icm'         => 'Models\Sp_pacemaker_icd_icm',
        'tracheostomy'          => 'Models\Sp_tracheostomy',
        'hemodialysis'          => 'Models\Sp_hemodialysis',
        'o2-therapy'            => 'Models\Sp_o2_therapy',
        'imp-cardio-def'        => 'Models\Sp_imp_cardio_def',
        'peripheral-iv'         => 'Models\Sp_peripheral_iv',
        'picc-line'             => 'Models\Sp_picc_line',
        'cvad'                  => 'Models\Sp_cvad',
        'parenteral-nutrition'  => 'Models\Sp_parenteral_nutrition',
        'iv-medication'         => 'Models\Sp_iv_medication'
    ];

    CONST CATHETERCOLS = [ 
        'drainageMethod' => 'Drainage Method',
        'size' => 'Size',
        'balloon' => 'Balloon',
        'bedbag' => 'Connected to bedside bag',
        'changeFrequency' => 'Change Frequency',
        'note' => 'Note',
        'discontinue_date' => 'Discontinue Date'
    ];

    CONST PACEMAKERCOLS = [
        'brand' => 'Brand/Model',
        'rateSetting' => 'Rate Setting',
        'note' => 'Note',
        'discontinue_date' => 'Discontinue Date'
    ];

    CONST VENOUSACCESS = [
        'type' => 'Brand/Type',
        'accSize' => 'Gauge size',
        'siteFreqChange' => 'Site frequency change',
        'note' => 'Note',
        'discontinue_date' => 'Discontinue Date'
    ];

    CONST COLS = [
        'indwelling'            => self::CATHETERCOLS,
        'suprapubic'            => self::CATHETERCOLS,
        'int-self'              => self::CATHETERCOLS,
        'int-irrigation'        => self::CATHETERCOLS,
        'urethral'              => self::CATHETERCOLS,
        'enteral-feeding'       => [
            'via' => 'Via',
            'size' => 'Size',
            'formula' => 'Formula',
            'amount24hrs' => 'Amount/Day',
            'additionalFluid' => 'Additional Fluid',
            'amountDay' => 'Calories/Day',
            'brandModel' => 'Brand/Model',
            'note' => 'Note',
            'mode' => 'Mode',
            'hoursDay' => 'Hours/Day',
            'rateHour' => 'Rate/Hour',
            'discontinue_date' => 'Discontinue Date'
        ],
        'ostomy'                => [
            'ostomy' => 'Ostomy',
            'siteCon' => 'Site Condition',
            'changeFrequency' => 'Ostomy bag change frequency',
            'careBy' => 'Ostomy care done by',
            'note' => 'Note',
            'discontinue_date' => 'Discontinue Date'
        ],
        'peritoneal-dialysis'   => [
            'peritoneal' => 'Solution/Schedule',
            'note' => 'Note',
            'discontinue_date' => 'Discontinue Date'
        ],
        'ventilator'            => [
            'ventilator' => 'Ventilator',
            'settings' => 'Settings',
            'fio2' => 'FiO2',
            'peep' => 'PEEP',
            'tv' => 'TV',
            'ac' => 'AC',
            'note' => 'Note',
            'discontinue_date' => 'Discontinue Date'
        ],
        'pacemaker'             => self::PACEMAKERCOLS,
        'pacemaker-icd'         => self::PACEMAKERCOLS,
        'pacemaker-icm'         => self::PACEMAKERCOLS,
        'imp-cardio-def'        => [
            'icd' => 'Implantable Cardioverter Defibrillator (ICD)',
            'date' => 'Date implanted',
            'note' => 'Note',
            'discontinue_date' => 'Discontinue Date'
        ],
        'tracheostomy'        => [
            'brand' => 'Tracheostomy brand/mode',
            'size' => 'Size',
            'note' => 'Note',
            'discontinue_date' => 'Discontinue Date'
        ],
        'hemodialysis'        => [
            'hemodialysis' => 'Times per week',
            'schedule' => 'Schedule',
            'note' => 'Note',
            'discontinue_date' => 'Discontinue Date'
        ],
        'o2-therapy'        => [
            'lpm' => 'LPM',
            'via' => 'O2 Via',
            'oxygenType' => 'Continuous/As needed',
            'note' => 'Note',
            'discontinue_date' => 'Discontinue Date'
        ],
        'peripheral-iv'     => self::VENOUSACCESS,
        'picc-line'         => self::VENOUSACCESS,
        'cvad'              => self::VENOUSACCESS,
        'parenteral-nutrition'=> [
            'parenteralNutrition'      => 'Solution/Rate/Duration',
            'note' => 'Note',
            'discontinue_date' => 'Discontinue Date'
        ],
        'iv-medication'=> [
            'medication'      => 'Medication/Frequency/Duration',
            'note' => 'Note',
            'discontinue_date' => 'Discontinue Date'
        ]
    ];

    CONST CATHETERTEXTADDONS = [
        'drainageMethod' => 'Insert $(data) Foley catheter',
        'size' => 'Fr $(data)',
        'balloon' => '$(data) ml balloon',
        'bedbag' => '$(data)',
        'changeFrequency' => '$(data)'
    ];

    CONST TEXTADDONS = [
        'indwelling'            => self::CATHETERTEXTADDONS,
        'suprapubic'            => self::CATHETERTEXTADDONS,
        'int-self'              => self::CATHETERTEXTADDONS,
        'int-irrigation'        => self::CATHETERTEXTADDONS,
        'enteral-feeding'       => [],
        'ostomy'                => [],
        'peritoneal-dialysis'   => [],
        'ventilator'            => [
            'ventilator'        => "$(data)",
            'settings'          => "Rate - $(data)",
            'fio2'              => "FI02 - $(data)",
            'peep'              => "PEEP - $(data)",
            'tv'                => "Tidal volume (Vt) - $(data)",
            'ac'                => "AC - $(data)"
        ],
        'pacemaker'             => [
            'brand'         => "$(data)",
            'rateSetting'   => "$(data)"
        ],
        'pacemaker_icd'         => [
            'brand'         => "$(data)",
            'rateSetting'   => "$(data)"
        ],
        'pacemaker_icm'         => [
            'brand'         => "$(data)",
            'rateSetting'   => "$(data)"
        ],
        'imp_cardio_def'        => [],
        'tracheostomy'          => [
            'brand'         => "$(data)",
            'size'          => "$(data)"
        ],
        'hemodialysis'          => [
            'schedule'      => "$(data)"
        ],
        'o2-therapy'            => [
            'lpm'           => "Administer O2 at $(data) liters/min",
            'via'           => "via $(data)",
            'oxygenType'    => "$(data)"
        ],
        'peripheral-iv'         => [],
        'picc-line'             => [],
        'cvad'                  => [],
        'parenteral-nutrition'  => [],
        'iv-medication'         => [],
    ];

    CONST CATHETERFLAGVAL = [
        'bedbag' => [
            'TRUE' => 'connect to bedside bag',
            'FALSE' => ''
        ]
    ];

    CONST FLAGVALUE = [
        'indwelling'            => self::CATHETERFLAGVAL,
        'suprapubic'            => self::CATHETERFLAGVAL,
        'int-self'              => self::CATHETERFLAGVAL,
        'int-irrigation'        => self::CATHETERFLAGVAL,
        // 'enteral-feeding'       => [],
        // 'ostomy'                => [],
        // 'peritoneal-dialysis'   => [],
        // 'ventilator'            => [
        //     'ventilator'        => "$(data)",
        //     'settings'          => "Rate - $(data)",
        //     'fio2'              => "FI02 - $(data)",
        //     'peep'              => "PEEP - $(data)",
        //     'tv'                => "Tidal volume (Vt) - $(data)",
        //     'ac'                => "AC - $(data)"
        // ],
        // 'pacemaker'             => [
        //     'brand'         => "$(data)",
        //     'rateSetting'   => "$(data)"
        // ],
        // 'pacemaker_icd'         => [
        //     'brand'         => "$(data)",
        //     'rateSetting'   => "$(data)"
        // ],
        // 'pacemaker_icm'         => [
        //     'brand'         => "$(data)",
        //     'rateSetting'   => "$(data)"
        // ],
        // 'imp_cardio_def'        => [],
        // 'tracheostomy'          => [
        //     'brand'         => "$(data)",
        //     'size'          => "$(data)"
        // ],
        // 'hemodialysis'          => [],
        // 'o2-therapy'            => [
        //     'lpm'           => "Administer O2 at $(data) liters/min",
        //     'via'           => "via $(data)",
        //     'oxygenType'    => "$(data)"
        // ],
        // 'peripheral-iv'         => [],
        // 'picc-line'             => [],
        // 'cvad'                  => [],
        // 'parenteral-nutrition'  => [],
        // 'iv-medication'         => [],
    ];

    CONST LISTTPL = [
        'indwelling'            => 'catheter',
        'suprapubic'            => 'catheter',
        'int-self'              => 'catheter',
        'int-irrigation'        => 'catheter',
        'enteral-feeding'       => 'enteral_feeding',
        'ostomy'                => 'ostomy',
        'peritoneal-dialysis'   => 'peritoneal_dialysis',
        'ventilator'            => 'ventilator',
        'pacemaker'             => 'pacemaker',
        'pacemaker-icd'         => 'pacemaker_icd_icm',
        'pacemaker-icm'         => 'pacemaker_icd_icm',
        'imp-cardio-def'        => 'imp_cardio_def',
        'tracheostomy'          => 'tracheostomy',
        'hemodialysis'          => 'hemodialysis',
        'o2-therapy'            => 'o2_therapy',
        'peripheral-iv'         => 'venous_access',
        'picc-line'             => 'venous_access',
        'cvad'                  => 'venous_access',
        'parenteral-nutrition'  => 'parenteral_nutrition',
        'iv-medication'         => 'iv_medication',
    ];

    CONST RBCOLS = [
        'indwelling'            => [],
        'suprapubic'            => [],
        'int-self'              => [],
        'int-irrigation'        => [],
        'enteral-feeding'       => [],
        'ostomy'                => [],
        'peritoneal-dialysis'   => [],
        'ventilator'            => [],
        'pacemaker'             => [],
        'pacemaker-icd'         => [],
        'pacemaker-icm'         => [],
        'imp-cardio-def'        => [],
        'tracheostomy'          => [],
        'hemodialysis'          => [],
        'o2-therapy'            => [],
        'peripheral-iv'         => [],
        'picc-line'             => [],
        'cvad'                  => [],
        'parenteral-nutrition'  => [],
        'iv-medication'         => [],
    ];

    // SPECIAL PROCEDURES CATEGORY
    CONST CARDIOPULMONARY = [
        "o2-therapy" => "O2 Therapy",
        "pacemaker" => "Pacemaker",
        'imp-cardio-def' => "Implantable Cardioverter Defibrillator (ICD)",
        "pacemaker-icd" => "Combination Pacemaker and ICD",
        "pacemaker-icm" => "Implantable Cardiac Monitor (ICM)",
        "tracheostomy" => "Tracheostomy",
        "ventilator" => "Ventilator",
    ];

    CONST ELIMINATION = [
        'indwelling'            => self::CATHETERTYPE['indwelling'],
        'suprapubic'            => self::CATHETERTYPE['suprapubic'],
        'int-self'              => self::CATHETERTYPE['int-self'],
        'int-irrigation'        => self::CATHETERTYPE['int-irrigation'],
        'urethral'              => self::CATHETERTYPE['urethral'],
        "ostomy"                => "Ostomy",
        'hemodialysis'          => 'Hemodialysis',
        "peritoneal-dialysis"   => "Peritoneal Dialysis"
    ];

    CONST NUTRITIONAL = [
        "enteral-feeding" => 'Enteral Feeding',
        "parenteral-nutrition" => 'Parenteral Nutrition'
    ];

    CONST MEDICATION = [
        "peripheral-iv" => "Peripheral IV",
        "picc-line" => "PICC Line",
        "cvad" => "CVAD",
        "iv-medication" => "IV Medication"
    ];

    CONST SPECIALPROCEDURES = [
        'cardiopulmonary'   => [ 
            'title' => 'Cardiopulmonary', 
            'list'  => self::CARDIOPULMONARY 
        ],
        'elimination'       => [
            'title' => 'Elimination', 
            'list'  => self::ELIMINATION
        ],
        'nutritional'       => [
            'title' => 'Nutritional',
            'list'  => self::NUTRITIONAL
        ],
        'medication'        => [
            'title' => 'Medication',
            'list'  => self::MEDICATION
        ]
    ];
    // SPECIAL PROCEDURES CATEGORY

    public static function flip_catheter_type() {
        return array_flip(self::CATHETERTYPE);
    }

    public static function export() {
        return [
            'catheterType' => self::CATHETERTYPE,
            'list' => self::SPECIALPROCEDURES,
            'listTpl' => self::LISTTPL,
            "reverseCatheterType" => self::flip_catheter_type()
        ];
    }

    public static function get_sp_list_params() {

        return [
            //CARDIOPULMONARY
            [ 
                "model" => "Models\Sp_ventilator", 
                "var" => "ventilator", 
                "procedure" => 'ventilator', 
                "tpl" => 'ventilator',
                'category' => 'cardiopulmonary' 
            ],
            [ 
                "model" => "Models\Sp_pacemaker", 
                "var" => "pacemaker", 
                "procedure" => 'pacemaker', 
                "tpl" => 'pacemaker',
                'category' => 'cardiopulmonary' 
            ],
            [ 
                "model" => "Models\Sp_imp_cardio_def", 
                "var" => "imp_cardio_def", 
                "procedure" => 'imp-cardio-def', 
                "tpl" => 'imp_cardio_def',
                'category' => 'cardiopulmonary' 
            ],
            [ 
                "model" => "Models\Sp_pacemaker_icd_icm", 
                "var" => "pacemaker_icd_icm", 
                "procedure" => 'pacemaker_icd_icm', 
                "tpl" => 'pacemaker_icd_icm',
                'category' => 'cardiopulmonary' 
            ],
            [ 
                "model" => "Models\Sp_tracheostomy", 
                "var" => "tracheostomy", 
                "procedure" => 'tracheostomy', 
                "tpl" => 'tracheostomy',
                'category' => 'cardiopulmonary' 
            ],
            [ 
                "model" => "Models\Sp_o2_therapy", 
                "var" => "lpm", 
                "procedure" => 'o2-therapy', 
                "tpl" => 'o2_therapy',
                'category' => 'cardiopulmonary' 
            ],

            //ELIMINATION
            [ 
                "model" => "Models\Sp_catheter", 
                "var" => "sp_catheter", 
                "procedure" => 'drainageMethod', 
                "tpl" => 'catheter',
                'category' => 'elimination' 
            ],
            [   
                "model" => "Models\Sp_ostomy", 
                "var" => "ostomy", 
                "procedure" => 'ostomy', 
                "tpl" => 'ostomy',
                'category' => 'elimination' 
            ],
            [ 
                "model" => "Models\Sp_peritoneal_dialysis", 
                "var" => "peritoneal", 
                "procedure" => 'peritoneal-dialysis', 
                "tpl" => 'peritoneal_dialysis',
                'category' => 'elimination'  
            ],
            [ 
                "model" => "Models\Sp_hemodialysis", 
                "var" => "hemodialysis", 
                "procedure" => 'hemodialysis', 
                "tpl" => 'hemodialysis',
                'category' => 'elimination'  
            ],

            //NUTRITIONAL
            [ 
                "model" => "Models\Sp_enteral_feeding", 
                "var" => "sp_enteral_feeding", 
                "procedure" => 'enteral-feeding', 
                "tpl" => 'enteral_feeding',
                'category' => 'nutritional' 
            ],
            [ 
                "model" => "Models\Sp_parenteral_nutrition", 
                "var" => "parenteralNutrition", 
                "procedure" => 'parenteral-nutrition', 
                "tpl" => 'parenteral_nutrition',
                'category' => 'nutritional' 
            ],

            //MEDICATION
            [ 
                "model" => "Models\Sp_peripheral_iv", 
                "var" => "peripheral-iv", 
                "procedure" => 'peripheral-iv', 
                "tpl" => 'venous_access',
                'category' => 'medication' 
            ],
            [ 
                "model" => "Models\Sp_picc_line", 
                "var" => "picc-line", 
                "procedure" => 'picc-line', 
                "tpl" => 'venous_access',
                'category' => 'medication' 
            ],
            [ 
                "model" => "Models\Sp_cvad", 
                "var" => "cvad", 
                "procedure" => 'cvad', 
                "tpl" => 'venous_access',
                'category' => 'medication' 
            ],
            [ 
                "model" => "Models\Sp_iv_medication", 
                "var" => "iv-medication", 
                "procedure" => 'iv-medication', 
                "tpl" => 'iv_medication',
                'category' => 'medication' 
            ]
        ];  
    }
}
