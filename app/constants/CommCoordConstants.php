<?php

namespace Constants;

class CommCoordConstants
{
    const CODE = [
        'cc_notes',
        'cc_missed_visit',
        'case-30',
        'case-60',
        'case-discharge-ins',
        'case-discharge-sum',
        'case-con'
    ];

    const MODELS = [
        self::CODE[0]   => "Models\CCNote",
        self::CODE[1]   => "Models\CCMissedVisit",
        self::CODE[2]   => "CaseSummary\Models\Case_30_days",
        self::CODE[3]   => "Models\Sixty_days_summary",
        self::CODE[4]   => "CaseSummary\Models\Case_discharge_ins",
        self::CODE[5]   => "Models\Discharge_summary",
        self::CODE[6]   => "Models\Case_conference",
    ];

    const TABLES = [
        self::CODE[0]   => "cc_notes",
        self::CODE[1]   => "cc_missed_visit",
        self::CODE[2]   => "case_30_days",
        self::CODE[3]   => "sixty_days_summary",
        self::CODE[4]   => "case_discharge_ins",
        self::CODE[5]   => "discharge_summary",
        self::CODE[6]   => "case_conference",
    ];
}