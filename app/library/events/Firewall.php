<?php

namespace Events;

use Interfaces\IEvent as IEvent;
use Security\Wap;
class Firewall extends \Phalcon\Events\Manager implements IEvent {

    private $token;

    public function __construct($arg) {
        // Add Event to validate message
        //$this->handleEvent();
        $this->token=$arg;
    }

    /**
     * Setup an Event
     *
     * Phalcon event to make sure client sends a valid message
     * @return FALSE|void
     */
    public function beforeExecuteRoute($event, $app) {

        if($app->di->getShared('User')->loggedIn()){
            $wap = $app->di->getShared('wap');
            $wap->firewallCheck($app->di->getShared('User')->getUser()->id);
        }

    }
}
