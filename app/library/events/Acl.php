<?php

/**
 * Event that check JWT Authentication
 *
 * @package Events
 * @subpackage Api
 * @author Jete O'Keeffe
 * @version 1.0
 */

namespace Events;

use Interfaces\IEvent as IEvent;
use Security\AclRoles;
class Acl extends \Phalcon\Events\Manager implements IEvent {

    public function __construct() {
        // Add Event to validate message
        //$this->handleEvent();
    }

    /**
     * Setup an Event
     *
     * Phalcon event to make sure client sends a valid message
     * @return FALSE|void
     */
    public function beforeExecuteRoute($event, $app) {

        if($app->di->getShared('User')->loggedIn()){
            
            $memberid=$app->di->getShared('User')->getUser()->id;

            if($this->checkAcl($memberid, $app->getActiveHandler())) return true;

            AclRoles::singleUserAcl($app->di->getShared('User')->getUser()->id, $app->collections->getCollections(), '\Models\Memberroles');

            if($this->checkAcl($memberid, $app->getActiveHandler())) return true;

            throw new \Micro\Exceptions\HTTPExceptions(
                'ACL Record says your not allowed to access this route.',
                401,
                array(
                    'dev' => 'Accessing this route is not permitted.',
                    'internalCode' => 'NF5001',
                    'more' => 'Unauthorized event.'
                )
            );

        }

    }

    protected function checkAcl($memberid, $handler){
        $ah = serialize($handler[0]);
        preg_match('/"Controllers\\\([^"]+)"/', $ah, $out);
        if(AclRoles::getAcl( $memberid, $out[1], $handler[1])){
            return true;
        };
    }
}
