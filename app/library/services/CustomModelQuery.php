<?php

namespace Services;

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class CustomModelQuery
{
    private $_stmt;
    private $_class;
    private $_params;
    private $_model;
    private $_result;

    public function __contstruct() {

    } 

    private function genModel($model) {
        if(gettype($model) == 'string') {
            $this->_class = "Models\\" . $model . "";

            return new $this->_class;
        }

        return $model;
    }

    public function execute($stmt, $model, $params)
    {        

        if(empty($stmt) || empty($model)) {
            throw new \Micro\Exceptions\HTTPExceptions(
                (empty($stmt) ? 'Query Statement' : 'Model') . ' is required.',
                406,
                array(
                    'dev' => (empty($stmt) ? 'Query Statement' : 'Model') . ' is required.',
                    'internalCode' => 'CM0004',
                    'more' => array("Error1", "Erro2", "Erro3")
                )
            );
        }

        $this->_model = $this->genModel($model);
        $this->_stmt = $stmt;
        $this->_params = $params;

        $this->_result = $this->_model->getReadConnection()->query($this->_stmt, $this->_params);

        return new Resultset(null, $this->_model, $this->_result);
    }
}