<?php

/**
 * Small Micro application to run simple/rest based applications
 *
 * @package Application
 * @author Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html
 * @example
$app = new Micro();
$app->setConfig('/path/to/config.php');
$app->setAutoload('/path/to/autoload.php');
$app->get('/api/looks/1', function() { echo "Hi"; });
$app->finish(function() { echo "Finished"; });
$app->run();
 */

namespace Application;

use Interfaces\IRun as IRun;
use \Phalcon\Http\Request;

class Micro extends \Phalcon\Mvc\Micro implements IRun {

	/**
	 * Pages that doesn't require authentication
	 * @var array
	 */
	protected $_noAuthPages;

	/**`
	 * User level access
	 * @var array
	 */

	protected $_levels;

	/**
	 * APP PATH VALUE
	 */

	public $appDir;

	/**
	 * Constructor of the App
	 */

	public function __construct() {
		$this->_noAuthPages = array();
		$this->_levels = array();
	}

	/**
	 * Set Dependency Injector with configuration variables
	 *
	 * @throws Exception		on bad database adapter
	 * @param string $file		full path to configuration file
	 */
	public function setConfig($file, $conOasis) {



		if (!file_exists($file) || !file_exists($conOasis)) {
			throw new \Micro\Exceptions\HTTPExceptions(
				'API Error',
				500,
				array(
					'dev' => 'Unable to load config file',
					'internalCode' => 'NF1003',
					'more' => 'Check config location.'
				)
			);
		}

		$di = new \Phalcon\DI\FactoryDefault();


		$di->setShared('collections', new \Route\RouteLoader($this->appDir));

		// By its class name
		$di->set("request", new Request());

		$configFile = require $file;

		$configOasis = require $conOasis;

		$configFile = array_merge($configFile, $configOasis);

		$di->set('config', new \Phalcon\Config($configFile));

		$di->set('db', function() use ($di) {
			$type = strtolower($di->get('config')->database->adapter);
			$creds = array(
				'host' => $di->get('config')->database->host,
				'username' => $di->get('config')->database->username,
				'password' => $di->get('config')->database->password,
				'dbname' => $di->get('config')->database->name
			);

			if ($type == 'mysql') {
				$connection =  new \Phalcon\Db\Adapter\Pdo\Mysql($creds);
			} else if ($type == 'postgres') {
				$connection =  new \Phalcon\Db\Adapter\Pdo\Postgesql($creds);
			} else if ($type == 'sqlite') {
				$connection =  new \Phalcon\Db\Adapter\Pdo\Sqlite($creds);
			} else {
				throw new \Micro\Exceptions\HTTPExceptions(
					'API Error',
					500,
					array(
						'dev' => 'Bad database adapter.',
						'internalCode' => 'NF1002',
						'more' => 'Check system adapter availability'
					)
				);
			}

			return $connection;
		});

		/**
		 * If our request contains a body, it has to be valid JSON.  This parses the
		 * body into a standard Object and makes that vailable from the DI.  If this service
		 * is called from a function, and the request body is nto valid JSON or is empty,
		 * the program will throw an Exception.
		 */
		$di->setShared('requestBody', function() use ($di) {
			$in = file_get_contents('php://input');
			$in = json_decode($in, FALSE);

			// JSON body could not be parsed, throw exception
			if($in === null){
				throw new \Micro\Exceptions\HTTPExceptions(
					'There was a problem understanding the data sent to the server by the application.',
					409,
					array(
						'dev' => 'The JSON body sent to the server was unable to be parsed.',
						'internalCode' => 'REQ1000',
						'more' => ''
					)
				);
			}
			return $in;
		});

		$di->set('User', function () use ($di) {
			$user = new \Services\User($di);
			return $user;
		});

		$di->set('redisWrapper', function () use ($di) {
			$redis = new \Services\RedisWrapper();
			return $redis;
		});

		$di->set('ErrorMessage', new \Services\ErrorMessages());
		$di->set('Batch', new \Services\Batch());
		$di->set('Mailer', new \Services\Mailer($configFile));
		$di->set('userAgent', new \Services\UserAgent());
		$di->set('wap', new \Security\Wap());
		$di->set('CustomModelQuery', new \Services\CustomModelQuery());
		$di->set('modelsManager', function() {
			return new \Phalcon\Mvc\Model\Manager();
		});

		$this->setDI($di);
	}

	/**
	 * Set namespaces to tranverse through in the autoloader
	 *
	 * @link http://docs.phalconphp.com/en/latest/reference/loader.html
	 * @throws Exception
	 * @param string $file		map of namespace to directories
	 */
	public function setAutoload($file, $dir, $formAutoload) {
		if (!file_exists($file)) {
			throw new \Micro\Exceptions\HTTPExceptions(
				'API Error',
				500,
				array(
					'dev' => 'Unable to load autoloader files.',
					'internalCode' => 'NF1001',
					'more' => 'Check autoloader file config'
				)
			);
		}

		// Set dir to be used inside include file
		$namespaces = array_merge(include $file, include $formAutoload);

		$loader = new \Phalcon\Loader();

		$loader->registerClasses(
			array(
				"S3" => $dir . "/library/utilities/S3/S3.php"
			)
		);
		$loader->registerDirs( array( $dir . "/library/utilities/predis" ) );
		$loader->registerNamespaces($namespaces)->register();
	}

	/**
	 * Set Routes\Handlers for the application
	 *
	 * @throws Exception
	 * @param file			file thats array of routes to load
	 */
	public function setRoutes($file) {
		if (!file_exists($file)) {
			throw new \Micro\Exceptions\HTTPExceptions(
				'API Error',
				500,
				array(
					'dev' => 'Unable to load route files.',
					'internalCode' => 'NF1001',
					'more' => 'Check route collection or format'
				)
			);
		}

		foreach($this->collections->getCollections() as $collection){
			$this->mount($collection);
		}

		$this->_noAuthPages = $this->collections->getNoAuthPages();

		$rawbody = $this->request->getJsonRawBody();
		if(!empty($rawbody) && !$this->request->isGet()){
			$this->di->getShared('requestBody');
		}

	}

	/**
	 * Set events to be triggered before/after certain stages in Micro App
	 *
	 * @param object $event		events to add
	 */
	public function setEvents($event) {
		$events = new \Phalcon\Events\Manager();
		$events->enablePriorities(true);
		$prio = count($event);
		foreach($event as $listener){
			$events->attach('micro', $listener, $prio); // Set Priority
			$prio--;
		}
		$this->setEventsManager($events);

	}

	/**
	 *
	 */
	public function getUnauthenticated() {
		return $this->_noAuthPages;
	}
	/**
	 *
	 */
	public function getLevels() {
		return $this->_levels;
	}
	/**
	 * Main run block that executes the micro application
	 *
	 */
	public function run() {
		$this->response->setHeader('Access-Control-Allow-Origin', '*');
		$this->response->sendHeaders();

		// Access-Control headers are received during OPTIONS requests
		if ($this->request->isOptions()) {
			$this->response->setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
			$this->response->setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
			$this->response->send();
		}


		$this->after(function(){

			// Results returned from the route's controller.  All Controllers should return an array
			$records = $this->getReturnedValue();

			if(empty($records)){

				throw new \Micro\Exceptions\HTTPExceptions(
					'No available response to your request.',
					412,
					array(
						'dev' => 'Returned empty set',
						'internalCode' => 'HC0000',
						'more' => ''
					)
				);
			}

			if(array_key_exists('file',$records)){
				// echo $records['file'];
				// return;
				$response = new \Micro\Responses\NonjsonResponse();
				$response->send($records);
				return;
			}

			$response = new \Micro\Responses\JSONResponse();
			$response->useEnvelope(true)
				->convertSnakeCase(true)
				->send($records);
			return;

		});

		// Handle any routes not found
		$this->notFound(function () {
			throw new \Micro\Exceptions\HTTPExceptions(
				'That route was not found on the server.',
				404,
				array(
					'dev' => 'That route was not found on the server.',
					'internalCode' => 'NF1000',
					'more' => 'Check route for misspellings.'
				)
			);
		});

		$this->handle();

	}

}
