<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\PresenceOf;

class Medicationteaching extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("title", new PresenceOf(["message" => "Title is required"]));
        $this->add("body", new PresenceOf(["message" => "Body is required"]));
        $this->add("in_rxcui", new PresenceOf(["message" => "Medication rxcui is required"]));
        // $this->add("idpatient", new PresenceOf(["message" => "Patient id is required"]));
    }

}