<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Models\Members;

class UserEmail extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("email", new PresenceOf(["message" => "Email is required"]));
        $this->add("email", new Email(["message" => "The e-mail is not valid"]));
    }

}