<?php

namespace Route;

/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 02/08/2016
 * Time: 6:53 PM
 */

class RouteLoader
{
    /**
     * Pages that doesn't require authentication
     * @var array
     */
    protected $_noAuthPages;

    /**`
     * User level access
     * @var array
     */

    public $collections;

    /**
     * APP PATH VALUE
     */

    public $appDir;

    /**
     * Constructor of the App
     */
    public function __construct($appDir){
        $this->_noAuthPages = array();
        $this->appDir = $appDir;
        $this->routeLoader();
    }
    /**
     * Route Loader
     */
    private function routeLoader(){
        $collections = array();
        $collectionFiles = scandir($this->appDir . '/routes');
        foreach($collectionFiles as $collectionFile){
            $pathinfo = pathinfo($collectionFile);
            //Only include php files
            if($pathinfo['extension'] === 'php' && $collectionFile != 'RouteLoader.php'){
                // The collection files return their collection objects, so mount
                // them directly into the router.
                $collections[] = $this->microCollection(include($this->appDir.'/routes/'.$collectionFile));
            }
        }
        $this->collections = $collections;
        return $collections;
    }
    /**
     * Micro Collection
     */
    private function microCollection($route){
        $col = new \Phalcon\Mvc\Micro\Collection();
        $col
            // VERSION NUMBER SHOULD BE FIRST URL PARAMETER, ALWAYS
            ->setPrefix($route['prefix'])
            // Must be a string in order to support lazy loading
            ->setHandler($route['handler'])
            ->setLazy($route['lazy']);

        foreach($route['collection'] as $r){
            //Store unauthenticated Collection
            if($r['authentication']===false){
                $method = strtolower($r['method']);
                if (! isset($this->_noAuthPages[$method])) {
                    $this->_noAuthPages[$method] = array();
                }
                $this->_noAuthPages[$method][] = $route['prefix'].$r['route'];
            }
            $col->{$r['method']}($r['route'], $r['function'], isset($r['resource']) ? $r['resource']:NULL);
        }
        return $col;
    }
    public function getCollections(){
        return $this->collections;
    }
    public function getNoAuthPages(){
        return $this->_noAuthPages;
    }
}