<?php
/**
 * @author GEEKSNEST
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.
 */
return [
    "prefix" => "/v1/tradoc",
    "handler" => 'Controllers\TradocController',
    "lazy" => true,
    "collection" => [
        [
            'method' => 'post',
            'route' => '/register',
            'function' => 'registerAction',
            'authentication' => FALSE,
            'resource' => 'rl1'
        ],
        [
            'method' => 'get',
            'route' => '/list',
            'function' => 'listRegister',
            'authentication' => FALSE,
            'resource' => 'rl1'
        ]

    ]
];
